import * as i0 from '@angular/core';
import { Component, Input, ViewChild, NgModule, HostListener } from '@angular/core';
import * as i2 from '@angular/common';
import { CommonModule } from '@angular/common';
import * as i1 from 'cres-com-wrapper';
import { AnalogSigObserver, DigitalSigObserver, CresComWrapperModule } from 'cres-com-wrapper';
import { trigger, transition, style, animate } from '@angular/animations';
import { take } from 'rxjs/operators';

class M3IndicatorBarComponent {
    constructor(renderer, changeDetection) {
        this.renderer = renderer;
        this.changeDetection = changeDetection;
        this.FBSignal = '';
        this.FBClass = 'primary';
        this.Direction = 'horizontal';
        this.MinValue = 0;
        this.MaxValue = 65535;
        this.lastBarValue = -1;
    }
    ngOnInit() {
        this.changeDetection.detach();
    }
    ngAfterViewInit() {
        this.changeDetection.detectChanges();
        this.barSubHandel = AnalogSigObserver
            .Observe(this.FBSignal)
            .subscribe((val) => {
            let newVal = this.scaleValue(val); //convert it to 0.0%-100.0%
            if (newVal !== this.lastBarValue) { //only run change detection on new values
                this.lastBarValue = newVal;
                let direction = this.Direction === 'horizontal' ? 'width' : 'height';
                this.renderer.setStyle(this.BarObj.nativeElement, direction, `${this.lastBarValue}%`);
                this.changeDetection.detectChanges();
            }
        });
    }
    ngOnDestroy() {
        this.barSubHandel?.unsubscribe();
    }
    //scale to 0.0-100.0%
    scaleValue(val) {
        let pct = (val - this.MinValue) / (this.MaxValue - this.MinValue);
        return Math.round(pct * 1000) / 10;
    }
}
M3IndicatorBarComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarComponent, deps: [{ token: i0.Renderer2 }, { token: i0.ChangeDetectorRef }], target: i0.ɵɵFactoryTarget.Component });
M3IndicatorBarComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: M3IndicatorBarComponent, selector: "m3-indicator-bar", inputs: { FBSignal: "FBSignal", FBClass: "FBClass", Direction: "Direction", MinValue: "MinValue", MaxValue: "MaxValue" }, viewQueries: [{ propertyName: "BarObj", first: true, predicate: ["barobj"], descendants: true }], ngImport: i0, template: "<div class=\"obj-bar-container\" [ngClass]=\"Direction\">\n  <div class=\"obj-bar-statics\" #barobj [ngClass]=\"FBClass\"></div>\n</div>\n", styles: [".obj-bar-container{height:100%;width:100%}.obj-bar-statics{height:100%}.vertical{display:flex;flex-direction:column;justify-content:flex-end}.horizontal{display:block}.primary{background-color:var(--m3-component-primary)}.accent{background-color:var(--m3-component-accent)}.warn{background-color:var(--m3-component-warn)}\n"], directives: [{ type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarComponent, decorators: [{
            type: Component,
            args: [{ selector: 'm3-indicator-bar', template: "<div class=\"obj-bar-container\" [ngClass]=\"Direction\">\n  <div class=\"obj-bar-statics\" #barobj [ngClass]=\"FBClass\"></div>\n</div>\n", styles: [".obj-bar-container{height:100%;width:100%}.obj-bar-statics{height:100%}.vertical{display:flex;flex-direction:column;justify-content:flex-end}.horizontal{display:block}.primary{background-color:var(--m3-component-primary)}.accent{background-color:var(--m3-component-accent)}.warn{background-color:var(--m3-component-warn)}\n"] }]
        }], ctorParameters: function () { return [{ type: i0.Renderer2 }, { type: i0.ChangeDetectorRef }]; }, propDecorators: { FBSignal: [{
                type: Input
            }], FBClass: [{
                type: Input
            }], Direction: [{
                type: Input
            }], MinValue: [{
                type: Input
            }], MaxValue: [{
                type: Input
            }], BarObj: [{
                type: ViewChild,
                args: ['barobj']
            }] } });

class M3IndicatorBarModule {
}
M3IndicatorBarModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
M3IndicatorBarModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarModule, declarations: [M3IndicatorBarComponent], imports: [CommonModule], exports: [M3IndicatorBarComponent] });
M3IndicatorBarModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        M3IndicatorBarComponent
                    ],
                    imports: [
                        CommonModule
                    ],
                    exports: [
                        M3IndicatorBarComponent
                    ]
                }]
        }] });

class M3TrayButtonComponent {
    constructor() {
        this.Icon = "";
        this.Label = "";
        this.Signal = "";
        this.FBSignal = "";
        this.FbClass = 'primary';
        this.circleWidth = 0;
    }
    onResize(event) {
        this.calculateCircleWidth();
    }
    calculateCircleWidth() {
        this.circleWidth = this.circleContainer?.nativeElement.offsetHeight;
    }
    getCircleWidth() {
        return { "width": `${this.circleWidth}px` };
    }
    ngOnInit() {
        this.btnFb = DigitalSigObserver.Observe(this.FBSignal);
    }
    ngAfterViewInit() {
        this.calculateCircleWidth();
    }
    buildFbClass() {
        switch (this.FbClass) {
            case 'primary':
                return 'fb-primary';
            case 'accent':
                return 'fb-accent';
            case 'warn':
                return 'fb-warn';
            default:
                return this.FbClass;
        }
    }
}
M3TrayButtonComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
M3TrayButtonComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: M3TrayButtonComponent, selector: "m3-tray-button", inputs: { Icon: "Icon", Label: "Label", Signal: "Signal", FBSignal: "FBSignal", FbClass: "FbClass" }, host: { listeners: { "window:resize": "onResize($event)" } }, viewQueries: [{ propertyName: "circleContainer", first: true, predicate: ["circleContainer"], descendants: true }], ngImport: i0, template: "<button CresDigitalSend [Signal]=\"Signal\">\n  <div class=\"circle-container\" #circleContainer [ngStyle]=\"getCircleWidth()\">\n    <div class=\"circle\">\n      <svg viewBox=\"0 0 15 15\">\n        <text x=\"1\" y=\"14\">{{Icon}}</text>\n      </svg>\n    </div>\n    <div *ngIf=\"btnFb | async\" [@enterAnimation] [ngClass]=\"buildFbClass()\" class=\"feedback-circle\">\n\n    </div>\n  </div>\n  <h6>{{Label}}</h6>\n</button>", styles: ["button{height:100%;width:inherit;background-color:transparent;border:none;padding:5px;color:var(--m3-component-primary-contrast);display:flex;flex-direction:column;justify-content:space-between;align-items:center}button h6{margin-top:4px;height:25px}.circle{position:absolute;height:100%;width:100%;border-radius:100%;z-index:1;margin:auto;border:1px solid var(--m3-component-primary-contrast);display:flex;justify-content:center;align-items:center}.feedback-circle{position:absolute;height:100%;width:100%;border-radius:100%}.circle-container{height:calc(100% - 25px);position:relative}.fb-primary{background-color:var(--m3-component-primary)}.fb-accent{background-color:var(--m3-component-accent)}.fb-warn{background-color:var(--m3-component-warn)}svg{height:60%;fill:var(--m3-component-primary-contrast)}text{font-family:\"Material Icons\",sans-serif}h6{text-transform:uppercase}\n"], directives: [{ type: i1.CresDigitalSendDirective, selector: "[CresDigitalSend]", inputs: ["Signal", "PressHold"] }, { type: i2.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], pipes: { "async": i2.AsyncPipe }, animations: [
        trigger('enterAnimation', [
            transition(':enter', [
                style({ transform: 'translateY(10%) scale(.9,.9)', opacity: 0 }),
                animate('200ms ease-in-out', style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }))
            ]),
            transition(':leave', [
                style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }),
                animate('200ms ease-in-out', style({ transform: 'translateY(6%) scale(.9,.9)', opacity: 0 }))
            ])
        ])
    ] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonComponent, decorators: [{
            type: Component,
            args: [{ selector: 'm3-tray-button', animations: [
                        trigger('enterAnimation', [
                            transition(':enter', [
                                style({ transform: 'translateY(10%) scale(.9,.9)', opacity: 0 }),
                                animate('200ms ease-in-out', style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }))
                            ]),
                            transition(':leave', [
                                style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }),
                                animate('200ms ease-in-out', style({ transform: 'translateY(6%) scale(.9,.9)', opacity: 0 }))
                            ])
                        ])
                    ], template: "<button CresDigitalSend [Signal]=\"Signal\">\n  <div class=\"circle-container\" #circleContainer [ngStyle]=\"getCircleWidth()\">\n    <div class=\"circle\">\n      <svg viewBox=\"0 0 15 15\">\n        <text x=\"1\" y=\"14\">{{Icon}}</text>\n      </svg>\n    </div>\n    <div *ngIf=\"btnFb | async\" [@enterAnimation] [ngClass]=\"buildFbClass()\" class=\"feedback-circle\">\n\n    </div>\n  </div>\n  <h6>{{Label}}</h6>\n</button>", styles: ["button{height:100%;width:inherit;background-color:transparent;border:none;padding:5px;color:var(--m3-component-primary-contrast);display:flex;flex-direction:column;justify-content:space-between;align-items:center}button h6{margin-top:4px;height:25px}.circle{position:absolute;height:100%;width:100%;border-radius:100%;z-index:1;margin:auto;border:1px solid var(--m3-component-primary-contrast);display:flex;justify-content:center;align-items:center}.feedback-circle{position:absolute;height:100%;width:100%;border-radius:100%}.circle-container{height:calc(100% - 25px);position:relative}.fb-primary{background-color:var(--m3-component-primary)}.fb-accent{background-color:var(--m3-component-accent)}.fb-warn{background-color:var(--m3-component-warn)}svg{height:60%;fill:var(--m3-component-primary-contrast)}text{font-family:\"Material Icons\",sans-serif}h6{text-transform:uppercase}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { circleContainer: [{
                type: ViewChild,
                args: ['circleContainer']
            }], Icon: [{
                type: Input
            }], Label: [{
                type: Input
            }], Signal: [{
                type: Input
            }], FBSignal: [{
                type: Input
            }], FbClass: [{
                type: Input
            }], onResize: [{
                type: HostListener,
                args: ['window:resize', ['$event']]
            }] } });

class M3TrayButtonModule {
}
M3TrayButtonModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
M3TrayButtonModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonModule, declarations: [M3TrayButtonComponent], imports: [CommonModule,
        CresComWrapperModule], exports: [M3TrayButtonComponent] });
M3TrayButtonModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonModule, imports: [[
            CommonModule,
            CresComWrapperModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        M3TrayButtonComponent
                    ],
                    imports: [
                        CommonModule,
                        CresComWrapperModule
                    ],
                    exports: [
                        M3TrayButtonComponent
                    ]
                }]
        }] });

class M3TrayPopupButtonComponent {
    constructor(rootElement, renderer, changeDetection, zone) {
        this.rootElement = rootElement;
        this.renderer = renderer;
        this.changeDetection = changeDetection;
        this.zone = zone;
        this.Icon = "";
        this.Label = "";
        this.Signal = "";
        this.FBSignal = "";
        this.FbClass = 'primary';
        this.windowLocation = 0;
        this.arrowOffset = 0;
        this.controlAreaOffset = 0;
        //viewportWidth:number = 0;
        this.controlAreaWidth = 0;
        this.circleWidth = 0;
        this.showPopup = false;
    }
    //Get the location for the full volume popup
    onResize(event) {
        //this.viewportWidth = window.innerWidth;
        //only updatew the popup if it is visable
        if (this.showPopup)
            this.rebuildPositionLocations();
    }
    //used to detect changes in NgContent
    onNgContentChange(event) {
        if (this.controlAreaWidth !== this.controlArea?.nativeElement.offsetWidth)
            this.onResize();
    }
    ngOnInit() {
        this.changeDetection.detach();
    }
    ngAfterViewInit() {
        this.changeDetection.detectChanges(); //initial change detection pass
        //set the width of the circle as we can't use the aspect-ratio css class on a TSW
        this.calculateCircleWidth();
        this.changeDetection.detectChanges();
        //subscribe to the CS signal and pass it to the update function
        this.btnFb = DigitalSigObserver.Observe(this.FBSignal);
        this.btnSub = this.btnFb.subscribe((val) => {
            this.UpdatePopupVisability(val);
        });
    }
    //manages the hiding and showing of the popup
    UpdatePopupVisability(state) {
        //update the popup and then run change detection
        this.showPopup = state;
        this.changeDetection.detectChanges();
        //then we need to figure out this popup
        if (this.showPopup) {
            //wait until the zone has setteled before starting the position update
            this.zone.onMicrotaskEmpty.asObservable().pipe(take(1)).subscribe(() => {
                this.rebuildPositionLocations();
            });
        }
        else {
            //zero out these values so they are recalculated on the next window showing
            this.windowLocation = 0;
            this.arrowOffset = 0;
            this.controlAreaOffset = 0;
            this.controlAreaWidth = 0;
        }
    }
    ngOnDestroy() {
        this.btnSub?.unsubscribe();
    }
    //Build locations for the popup window and arrow.
    rebuildPositionLocations() {
        //Get button location, width and viewport parameters
        let buttonStart = this.GetElementXPosition(this.ButtonElement?.nativeElement);
        let buttonWidth = this.ButtonElement?.nativeElement.offsetWidth;
        let controlWidth = this.controlArea?.nativeElement.offsetWidth;
        let controlContainerWidth = this.volumePopup?.nativeElement.offsetWidth;
        let changeCount = 0;
        changeCount += (this.calculateWindowLocation(buttonStart) ? 1 : 0);
        changeCount += (this.calculateArrowLoaction(buttonStart, buttonWidth) ? 1 : 0);
        changeCount += (this.calculateControlAreaPosition(buttonStart, buttonWidth, controlWidth, controlContainerWidth) ? 1 : 0);
        if (changeCount > 0) {
            this.changeDetection.detectChanges();
        }
    }
    //gets the window location, if it has changed, return true for chang detection to run
    calculateWindowLocation(elementStart) {
        //Window Loacation is just -1*elementStart
        let newLocation = elementStart * -1;
        if (this.windowLocation !== newLocation) {
            this.windowLocation = newLocation;
            this.renderer.setStyle(this.volumePopup.nativeElement, 'margin-left', `${this.windowLocation}px`);
            return true;
        }
        return false;
    }
    //Analyse and calculate where the arrow should go, returns true if a change needs to be made
    calculateArrowLoaction(elementStart, elementWidth) {
        let arrowWidth = 20;
        //Arrow Offset is elementStart + 1/2 the with of the button - 1/2 the legnth of the arrow
        let newArrowOffset = elementStart + (elementWidth - arrowWidth) / 2;
        if (this.arrowOffset !== newArrowOffset) {
            this.arrowOffset = newArrowOffset;
            this.renderer.setStyle(this.arrowReff?.nativeElement, "margin-left", `${this.arrowOffset}px`);
            return true;
        }
        return false;
    }
    //Analyse the position of the control area, if a change needs to be made, returns true
    calculateControlAreaPosition(buttonStart, buttonWidth, controlAreaWidth, controlAreaContainerWidth) {
        let newControlAreaOffset = this.getControlAreaPosition(buttonStart, buttonWidth, controlAreaWidth, controlAreaContainerWidth);
        if (this.controlAreaOffset != newControlAreaOffset) {
            this.controlAreaOffset = newControlAreaOffset;
            this.renderer.setStyle(this.controlArea.nativeElement, "margin-left", `${this.controlAreaOffset}px`);
            return true;
        }
        return false;
    }
    //solve for where the window needs to go, returns a px from the left where the window starts
    getControlAreaPosition(buttonStart, buttonWidth, controlAreaWidth, controlAreaContainerWidth) {
        let buttonCenter = buttonStart + (buttonWidth / 2);
        let halfControlWidth = controlAreaWidth / 2;
        //if it can be centered, just do that...
        if (buttonCenter - halfControlWidth > 30 &&
            (controlAreaContainerWidth - buttonCenter) - halfControlWidth > 30) {
            return buttonCenter - halfControlWidth;
        }
        //if it can't be centered, we pin to the left or the right
        //button is closer to the right, so pin it 30 px form the right
        if (buttonCenter > (controlAreaContainerWidth / 2)) {
            return controlAreaContainerWidth - controlAreaWidth - 30;
        }
        //its closer to the left so pin 30 px to the left
        return 30;
    }
    calculateCircleWidth() {
        let newWidth = this.circleContainer?.nativeElement.offsetHeight;
        if (this.circleWidth !== newWidth) {
            this.circleWidth = newWidth;
            this.renderer.setStyle(this.circleContainer?.nativeElement, 'width', `${this.circleWidth}px`);
            return true;
        }
        return false;
    }
    buildFbClass() {
        switch (this.FbClass) {
            case 'primary':
                return 'fb-primary';
            case 'accent':
                return 'fb-accent';
            case 'warn':
                return 'fb-warn';
            default:
                return this.FbClass;
        }
    }
    //function to recursivly get the offsetLeft of all parent containers, returns the sum
    GetElementXPosition(element) {
        let position = 0;
        if (element.offsetParent !== null) {
            position = this.GetElementXPosition(element.offsetParent);
        }
        position += element.offsetLeft;
        return position;
    }
}
M3TrayPopupButtonComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonComponent, deps: [{ token: i0.ElementRef }, { token: i0.Renderer2 }, { token: i0.ChangeDetectorRef }, { token: i0.NgZone }], target: i0.ɵɵFactoryTarget.Component });
M3TrayPopupButtonComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: M3TrayPopupButtonComponent, selector: "m3-tray-popup-button", inputs: { Icon: "Icon", Label: "Label", Signal: "Signal", FBSignal: "FBSignal", FbClass: "FbClass" }, host: { listeners: { "window:resize": "onResize($event)" } }, viewQueries: [{ propertyName: "arrowReff", first: true, predicate: ["arrow"], descendants: true }, { propertyName: "controlArea", first: true, predicate: ["ControlArea"], descendants: true }, { propertyName: "volumePopup", first: true, predicate: ["volumePopup"], descendants: true }, { propertyName: "circleContainer", first: true, predicate: ["circleContainer"], descendants: true }, { propertyName: "ButtonElement", first: true, predicate: ["button"], descendants: true }], ngImport: i0, template: "<!--Floating volume window that is drawn above the tray-->\n<!--Volume popup takes full viewport width width, used to center actual volume controls-->\n<div class=\"volume-popup\"\n    #volumePopup\n    [@enterAnimation]\n    *ngIf=\"showPopup\">\n\n    <!--Content container,sized based on input from ng-content-->\n    <div \n      #ControlArea \n      class=\"volume-control-area\"\n      (cdkObserveContent)=\"onNgContentChange($event)\">\n      <ng-content></ng-content>\n    </div>\n\n  <div class=\"pointer-arrow-container\">\n    <!--Inline SVG down arrow-->\n    <div #arrow class=\"pointer-arrow\">\n      <svg viewBox=\"0 0 100 50\" class=\"triangle\">\n\t\t    <polygon points=\"0,0 100,0 50,50\"/>\n\t    </svg>\n    </div>  \n  </div>\n</div>\n\n<!--The actual tray button-->\n<button #button CresDigitalSend [Signal]=\"Signal\">\n  <div #circleContainer class=\"circle-container\">\n    <div class=\"circle\">\n      <svg viewBox=\"0 0 15 15\">\n        <text x=\"1\" y=\"14\">{{Icon}}</text>\n      </svg>\n    </div>\n    <div *ngIf=\"showPopup\" [@enterAnimation] [ngClass]=\"buildFbClass()\" class=\"feedback-circle\">\n    </div>\n  </div>\n  <h6>{{Label}}</h6>\n</button>\n", styles: [":host{overflow:visible;position:relative;height:100%}.volume-popup{position:absolute;bottom:105%;z-index:15;height:60vh;width:100vw;display:flex;flex-direction:column;justify-content:center}.pointer-arrow-container{height:10px;width:100%}.pointer-arrow{height:100%;width:20px;display:flex;justify-content:center}.triangle{fill:var(--m3-component-primary-50);height:10px;width:20px}.volume-control-area{height:calc(100% - 10px);min-width:60px;margin:auto;border-radius:5px;background-color:var(--m3-component-primary-50);color:var(--m3-component-primary-contrast-50)}button{height:100%;width:inherit;background-color:transparent;border:none;padding:5px;color:var(--m3-component-primary-contrast);display:flex;flex-direction:column;justify-content:space-between;align-items:center}button h6{margin-top:4px;height:25px}.circle{position:absolute;height:100%;width:100%;border-radius:100%;z-index:1;margin:auto;border:1px solid var(--m3-component-primary-contrast);display:flex;justify-content:center;align-items:center}.feedback-circle{position:absolute;height:100%;width:100%;border-radius:100%}.circle-container{height:calc(100% - 25px);position:relative}.fb-primary{background-color:var(--m3-component-primary)}.fb-accent{background-color:var(--m3-component-accent)}.fb-warn{background-color:var(--m3-component-warn)}svg{height:60%;fill:var(--m3-component-primary-contrast)}text{font-family:\"Material Icons\",sans-serif}h6{text-transform:uppercase}\n"], directives: [{ type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.CresDigitalSendDirective, selector: "[CresDigitalSend]", inputs: ["Signal", "PressHold"] }, { type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], animations: [
        trigger('enterAnimation', [
            transition(':enter', [
                style({ transform: 'translateY(10%) scale(.9,.9)', opacity: 0 }),
                animate('200ms ease-in-out', style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }))
            ]),
            transition(':leave', [
                style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }),
                animate('200ms ease-in-out', style({ transform: 'translateY(6%) scale(.9,.9)', opacity: 0 }))
            ])
        ])
    ] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonComponent, decorators: [{
            type: Component,
            args: [{ selector: 'm3-tray-popup-button', animations: [
                        trigger('enterAnimation', [
                            transition(':enter', [
                                style({ transform: 'translateY(10%) scale(.9,.9)', opacity: 0 }),
                                animate('200ms ease-in-out', style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }))
                            ]),
                            transition(':leave', [
                                style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }),
                                animate('200ms ease-in-out', style({ transform: 'translateY(6%) scale(.9,.9)', opacity: 0 }))
                            ])
                        ])
                    ], template: "<!--Floating volume window that is drawn above the tray-->\n<!--Volume popup takes full viewport width width, used to center actual volume controls-->\n<div class=\"volume-popup\"\n    #volumePopup\n    [@enterAnimation]\n    *ngIf=\"showPopup\">\n\n    <!--Content container,sized based on input from ng-content-->\n    <div \n      #ControlArea \n      class=\"volume-control-area\"\n      (cdkObserveContent)=\"onNgContentChange($event)\">\n      <ng-content></ng-content>\n    </div>\n\n  <div class=\"pointer-arrow-container\">\n    <!--Inline SVG down arrow-->\n    <div #arrow class=\"pointer-arrow\">\n      <svg viewBox=\"0 0 100 50\" class=\"triangle\">\n\t\t    <polygon points=\"0,0 100,0 50,50\"/>\n\t    </svg>\n    </div>  \n  </div>\n</div>\n\n<!--The actual tray button-->\n<button #button CresDigitalSend [Signal]=\"Signal\">\n  <div #circleContainer class=\"circle-container\">\n    <div class=\"circle\">\n      <svg viewBox=\"0 0 15 15\">\n        <text x=\"1\" y=\"14\">{{Icon}}</text>\n      </svg>\n    </div>\n    <div *ngIf=\"showPopup\" [@enterAnimation] [ngClass]=\"buildFbClass()\" class=\"feedback-circle\">\n    </div>\n  </div>\n  <h6>{{Label}}</h6>\n</button>\n", styles: [":host{overflow:visible;position:relative;height:100%}.volume-popup{position:absolute;bottom:105%;z-index:15;height:60vh;width:100vw;display:flex;flex-direction:column;justify-content:center}.pointer-arrow-container{height:10px;width:100%}.pointer-arrow{height:100%;width:20px;display:flex;justify-content:center}.triangle{fill:var(--m3-component-primary-50);height:10px;width:20px}.volume-control-area{height:calc(100% - 10px);min-width:60px;margin:auto;border-radius:5px;background-color:var(--m3-component-primary-50);color:var(--m3-component-primary-contrast-50)}button{height:100%;width:inherit;background-color:transparent;border:none;padding:5px;color:var(--m3-component-primary-contrast);display:flex;flex-direction:column;justify-content:space-between;align-items:center}button h6{margin-top:4px;height:25px}.circle{position:absolute;height:100%;width:100%;border-radius:100%;z-index:1;margin:auto;border:1px solid var(--m3-component-primary-contrast);display:flex;justify-content:center;align-items:center}.feedback-circle{position:absolute;height:100%;width:100%;border-radius:100%}.circle-container{height:calc(100% - 25px);position:relative}.fb-primary{background-color:var(--m3-component-primary)}.fb-accent{background-color:var(--m3-component-accent)}.fb-warn{background-color:var(--m3-component-warn)}svg{height:60%;fill:var(--m3-component-primary-contrast)}text{font-family:\"Material Icons\",sans-serif}h6{text-transform:uppercase}\n"] }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }, { type: i0.ChangeDetectorRef }, { type: i0.NgZone }]; }, propDecorators: { Icon: [{
                type: Input
            }], Label: [{
                type: Input
            }], Signal: [{
                type: Input
            }], FBSignal: [{
                type: Input
            }], FbClass: [{
                type: Input
            }], arrowReff: [{
                type: ViewChild,
                args: ['arrow']
            }], controlArea: [{
                type: ViewChild,
                args: ['ControlArea']
            }], volumePopup: [{
                type: ViewChild,
                args: ['volumePopup']
            }], circleContainer: [{
                type: ViewChild,
                args: ['circleContainer']
            }], ButtonElement: [{
                type: ViewChild,
                args: ['button']
            }], onResize: [{
                type: HostListener,
                args: ['window:resize', ['$event']]
            }] } });

class M3TrayPopupButtonModule {
}
M3TrayPopupButtonModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
M3TrayPopupButtonModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonModule, declarations: [M3TrayPopupButtonComponent], imports: [CommonModule,
        CresComWrapperModule], exports: [M3TrayPopupButtonComponent] });
M3TrayPopupButtonModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonModule, imports: [[
            CommonModule,
            CresComWrapperModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        M3TrayPopupButtonComponent
                    ],
                    imports: [
                        CommonModule,
                        CresComWrapperModule
                    ],
                    exports: [
                        M3TrayPopupButtonComponent
                    ]
                }]
        }] });

/*
 * Public API Surface of m3-components
 */

/**
 * Generated bundle index. Do not edit.
 */

export { M3IndicatorBarComponent, M3IndicatorBarModule, M3TrayButtonComponent, M3TrayButtonModule, M3TrayPopupButtonComponent, M3TrayPopupButtonModule };
