# M3-Components

M3 Components is a growing library of UI components designed to be used in Creston CH5 projects.

# Version History
- 0.2.1 - Added global styles for cres-com-wrapper
- 0.2.0 - Added Angular 13.x support
- 0.1.0 - Initial Release, Angular 12.x support

# Components


## Indicator Bar
| Type      | Name                    |
|-----------|-------------------------|
| Selector  | m3-indicator-bar        |
| ngModule  | M3IndicatorBarModule    |

Analog indicator bar that takes an analog signal and displays a vertical or horizontal gauge.
M3-indicator bar component should be placed inside a div that is sized to the desired size of the bar. Change the Direction of the bar with the `direction` parameter

| Parameter | Description                                                                                                 |
|-----------|-------------------------------------------------------------------------------------------------------------|
| FBSignal  | Crestron Signal name or join number to subscribe to                                                         |
| FB Class  | CSS Class for bar color. use 'primary, 'accent' or 'warn' to reference global themes. Defaults to 'primary' |
| Direction | type of ('horizontal' | 'vertical'). Sets direction of indicator. Defaults to 'horizontal'                  |
| MinValue  | Expected minimum value of analog signal range. Defaults to 0.                                               |
| MaxValue  | Expected maximum value of analog signal range. Defaults to 65535.                                           |


## Tray Button
| Type      | Name                    |
|-----------|-------------------------|
| Selector  | m3-tray-button          |
| ngModule  | M3TrayButtonModule      |

Themed system tray button, provides a rising edge on push, accepts control system FB.

Button size is set by containing element height. Button will not render correctly if the parent element dosn't have a height.

| Parameter | Description                                                                                                         |
|-----------|---------------------------------------------------------------------------------------------------------------------|
| Signal    | Crestron Signal name or join number to send digital push event to                                                   |
| FBSignal  | Crestron Signal name or join number to subscribe to                                                                 |
| FB Class  | CSS Class for button on state. use 'primary, 'accent' or 'warn' to reference global themes. Defaults to 'primary'   |
| Label     | Text label to appear below the button                                                                               |
| Icon      | Material icon to be shown in the center of the button, use material font icon names: https://fonts.google.com/icons |

## Tray Button
| Type      | Name                    |
|-----------|-------------------------|
| Selector  | m3-tray-popup-button    |
| ngModule  | M3TrayPopupButtonModule |

Same as M3TrayButton, but can take ngContent and draw a popup window above the button when it's FB signal is high.

tray-popup-button is designed to be at the bottom ogf the screen. It will responsivly draw a popup button outside its containing element.

Button size is set by containing element height. Button will not render correctly if the parent element dosn't have a height.

| Parameter | Description                                                                                                         |
|-----------|---------------------------------------------------------------------------------------------------------------------|
| Signal    | Crestron Signal name or join number to send digital push event to                                                   |
| FBSignal  | Crestron Signal name or join number to subscribe to. Popup opens when this signal is high.                          |
| FB Class  | CSS Class for button on state. use 'primary, 'accent' or 'warn' to reference global themes. Defaults to 'primary'   |
| Label     | Text label to appear below the button                                                                               |
| Icon      | Material icon to be shown in the center of the button, use material font icon names: https://fonts.google.com/icons |
| ngContent | HTML Elements to render inside the popup when open.                                                                 |

