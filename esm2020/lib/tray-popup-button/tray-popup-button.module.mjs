import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CresComWrapperModule } from "cres-com-wrapper";
import { M3TrayPopupButtonComponent } from "./tray-popup-button.component";
import * as i0 from "@angular/core";
export class M3TrayPopupButtonModule {
}
M3TrayPopupButtonModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
M3TrayPopupButtonModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonModule, declarations: [M3TrayPopupButtonComponent], imports: [CommonModule,
        CresComWrapperModule], exports: [M3TrayPopupButtonComponent] });
M3TrayPopupButtonModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonModule, imports: [[
            CommonModule,
            CresComWrapperModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        M3TrayPopupButtonComponent
                    ],
                    imports: [
                        CommonModule,
                        CresComWrapperModule
                    ],
                    exports: [
                        M3TrayPopupButtonComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJheS1wb3B1cC1idXR0b24ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbTMtY29tcG9uZW50cy9zcmMvbGliL3RyYXktcG9wdXAtYnV0dG9uL3RyYXktcG9wdXAtYnV0dG9uLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUV4RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7QUFlM0UsTUFBTSxPQUFPLHVCQUF1Qjs7b0hBQXZCLHVCQUF1QjtxSEFBdkIsdUJBQXVCLGlCQVZoQywwQkFBMEIsYUFHMUIsWUFBWTtRQUNaLG9CQUFvQixhQUdwQiwwQkFBMEI7cUhBR2pCLHVCQUF1QixZQVJ6QjtZQUNQLFlBQVk7WUFDWixvQkFBb0I7U0FDckI7MkZBS1UsdUJBQXVCO2tCQVpuQyxRQUFRO21CQUFDO29CQUNSLFlBQVksRUFBRTt3QkFDWiwwQkFBMEI7cUJBQzNCO29CQUNELE9BQU8sRUFBRTt3QkFDUCxZQUFZO3dCQUNaLG9CQUFvQjtxQkFDckI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLDBCQUEwQjtxQkFDM0I7aUJBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBDcmVzQ29tV3JhcHBlck1vZHVsZSB9IGZyb20gXCJjcmVzLWNvbS13cmFwcGVyXCI7XHJcblxyXG5pbXBvcnQgeyBNM1RyYXlQb3B1cEJ1dHRvbkNvbXBvbmVudCB9IGZyb20gXCIuL3RyYXktcG9wdXAtYnV0dG9uLmNvbXBvbmVudFwiO1xyXG5cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBNM1RyYXlQb3B1cEJ1dHRvbkNvbXBvbmVudFxyXG4gIF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgQ3Jlc0NvbVdyYXBwZXJNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtcclxuICAgIE0zVHJheVBvcHVwQnV0dG9uQ29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTTNUcmF5UG9wdXBCdXR0b25Nb2R1bGUgeyB9XHJcbiJdfQ==