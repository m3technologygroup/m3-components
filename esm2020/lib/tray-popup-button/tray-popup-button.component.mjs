import { Component, Input, HostListener, ViewChild } from '@angular/core';
import { DigitalSigObserver } from 'cres-com-wrapper';
import { trigger, style, animate, transition } from '@angular/animations';
import { take } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "cres-com-wrapper";
export class M3TrayPopupButtonComponent {
    constructor(rootElement, renderer, changeDetection, zone) {
        this.rootElement = rootElement;
        this.renderer = renderer;
        this.changeDetection = changeDetection;
        this.zone = zone;
        this.Icon = "";
        this.Label = "";
        this.Signal = "";
        this.FBSignal = "";
        this.FbClass = 'primary';
        this.windowLocation = 0;
        this.arrowOffset = 0;
        this.controlAreaOffset = 0;
        //viewportWidth:number = 0;
        this.controlAreaWidth = 0;
        this.circleWidth = 0;
        this.showPopup = false;
    }
    //Get the location for the full volume popup
    onResize(event) {
        //this.viewportWidth = window.innerWidth;
        //only updatew the popup if it is visable
        if (this.showPopup)
            this.rebuildPositionLocations();
    }
    //used to detect changes in NgContent
    onNgContentChange(event) {
        if (this.controlAreaWidth !== this.controlArea?.nativeElement.offsetWidth)
            this.onResize();
    }
    ngOnInit() {
        this.changeDetection.detach();
    }
    ngAfterViewInit() {
        this.changeDetection.detectChanges(); //initial change detection pass
        //set the width of the circle as we can't use the aspect-ratio css class on a TSW
        this.calculateCircleWidth();
        this.changeDetection.detectChanges();
        //subscribe to the CS signal and pass it to the update function
        this.btnFb = DigitalSigObserver.Observe(this.FBSignal);
        this.btnSub = this.btnFb.subscribe((val) => {
            this.UpdatePopupVisability(val);
        });
    }
    //manages the hiding and showing of the popup
    UpdatePopupVisability(state) {
        //update the popup and then run change detection
        this.showPopup = state;
        this.changeDetection.detectChanges();
        //then we need to figure out this popup
        if (this.showPopup) {
            //wait until the zone has setteled before starting the position update
            this.zone.onMicrotaskEmpty.asObservable().pipe(take(1)).subscribe(() => {
                this.rebuildPositionLocations();
            });
        }
        else {
            //zero out these values so they are recalculated on the next window showing
            this.windowLocation = 0;
            this.arrowOffset = 0;
            this.controlAreaOffset = 0;
            this.controlAreaWidth = 0;
        }
    }
    ngOnDestroy() {
        this.btnSub?.unsubscribe();
    }
    //Build locations for the popup window and arrow.
    rebuildPositionLocations() {
        //Get button location, width and viewport parameters
        let buttonStart = this.GetElementXPosition(this.ButtonElement?.nativeElement);
        let buttonWidth = this.ButtonElement?.nativeElement.offsetWidth;
        let controlWidth = this.controlArea?.nativeElement.offsetWidth;
        let controlContainerWidth = this.volumePopup?.nativeElement.offsetWidth;
        let changeCount = 0;
        changeCount += (this.calculateWindowLocation(buttonStart) ? 1 : 0);
        changeCount += (this.calculateArrowLoaction(buttonStart, buttonWidth) ? 1 : 0);
        changeCount += (this.calculateControlAreaPosition(buttonStart, buttonWidth, controlWidth, controlContainerWidth) ? 1 : 0);
        if (changeCount > 0) {
            this.changeDetection.detectChanges();
        }
    }
    //gets the window location, if it has changed, return true for chang detection to run
    calculateWindowLocation(elementStart) {
        //Window Loacation is just -1*elementStart
        let newLocation = elementStart * -1;
        if (this.windowLocation !== newLocation) {
            this.windowLocation = newLocation;
            this.renderer.setStyle(this.volumePopup.nativeElement, 'margin-left', `${this.windowLocation}px`);
            return true;
        }
        return false;
    }
    //Analyse and calculate where the arrow should go, returns true if a change needs to be made
    calculateArrowLoaction(elementStart, elementWidth) {
        let arrowWidth = 20;
        //Arrow Offset is elementStart + 1/2 the with of the button - 1/2 the legnth of the arrow
        let newArrowOffset = elementStart + (elementWidth - arrowWidth) / 2;
        if (this.arrowOffset !== newArrowOffset) {
            this.arrowOffset = newArrowOffset;
            this.renderer.setStyle(this.arrowReff?.nativeElement, "margin-left", `${this.arrowOffset}px`);
            return true;
        }
        return false;
    }
    //Analyse the position of the control area, if a change needs to be made, returns true
    calculateControlAreaPosition(buttonStart, buttonWidth, controlAreaWidth, controlAreaContainerWidth) {
        let newControlAreaOffset = this.getControlAreaPosition(buttonStart, buttonWidth, controlAreaWidth, controlAreaContainerWidth);
        if (this.controlAreaOffset != newControlAreaOffset) {
            this.controlAreaOffset = newControlAreaOffset;
            this.renderer.setStyle(this.controlArea.nativeElement, "margin-left", `${this.controlAreaOffset}px`);
            return true;
        }
        return false;
    }
    //solve for where the window needs to go, returns a px from the left where the window starts
    getControlAreaPosition(buttonStart, buttonWidth, controlAreaWidth, controlAreaContainerWidth) {
        let buttonCenter = buttonStart + (buttonWidth / 2);
        let halfControlWidth = controlAreaWidth / 2;
        //if it can be centered, just do that...
        if (buttonCenter - halfControlWidth > 30 &&
            (controlAreaContainerWidth - buttonCenter) - halfControlWidth > 30) {
            return buttonCenter - halfControlWidth;
        }
        //if it can't be centered, we pin to the left or the right
        //button is closer to the right, so pin it 30 px form the right
        if (buttonCenter > (controlAreaContainerWidth / 2)) {
            return controlAreaContainerWidth - controlAreaWidth - 30;
        }
        //its closer to the left so pin 30 px to the left
        return 30;
    }
    calculateCircleWidth() {
        let newWidth = this.circleContainer?.nativeElement.offsetHeight;
        if (this.circleWidth !== newWidth) {
            this.circleWidth = newWidth;
            this.renderer.setStyle(this.circleContainer?.nativeElement, 'width', `${this.circleWidth}px`);
            return true;
        }
        return false;
    }
    buildFbClass() {
        switch (this.FbClass) {
            case 'primary':
                return 'fb-primary';
            case 'accent':
                return 'fb-accent';
            case 'warn':
                return 'fb-warn';
            default:
                return this.FbClass;
        }
    }
    //function to recursivly get the offsetLeft of all parent containers, returns the sum
    GetElementXPosition(element) {
        let position = 0;
        if (element.offsetParent !== null) {
            position = this.GetElementXPosition(element.offsetParent);
        }
        position += element.offsetLeft;
        return position;
    }
}
M3TrayPopupButtonComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonComponent, deps: [{ token: i0.ElementRef }, { token: i0.Renderer2 }, { token: i0.ChangeDetectorRef }, { token: i0.NgZone }], target: i0.ɵɵFactoryTarget.Component });
M3TrayPopupButtonComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: M3TrayPopupButtonComponent, selector: "m3-tray-popup-button", inputs: { Icon: "Icon", Label: "Label", Signal: "Signal", FBSignal: "FBSignal", FbClass: "FbClass" }, host: { listeners: { "window:resize": "onResize($event)" } }, viewQueries: [{ propertyName: "arrowReff", first: true, predicate: ["arrow"], descendants: true }, { propertyName: "controlArea", first: true, predicate: ["ControlArea"], descendants: true }, { propertyName: "volumePopup", first: true, predicate: ["volumePopup"], descendants: true }, { propertyName: "circleContainer", first: true, predicate: ["circleContainer"], descendants: true }, { propertyName: "ButtonElement", first: true, predicate: ["button"], descendants: true }], ngImport: i0, template: "<!--Floating volume window that is drawn above the tray-->\n<!--Volume popup takes full viewport width width, used to center actual volume controls-->\n<div class=\"volume-popup\"\n    #volumePopup\n    [@enterAnimation]\n    *ngIf=\"showPopup\">\n\n    <!--Content container,sized based on input from ng-content-->\n    <div \n      #ControlArea \n      class=\"volume-control-area\"\n      (cdkObserveContent)=\"onNgContentChange($event)\">\n      <ng-content></ng-content>\n    </div>\n\n  <div class=\"pointer-arrow-container\">\n    <!--Inline SVG down arrow-->\n    <div #arrow class=\"pointer-arrow\">\n      <svg viewBox=\"0 0 100 50\" class=\"triangle\">\n\t\t    <polygon points=\"0,0 100,0 50,50\"/>\n\t    </svg>\n    </div>  \n  </div>\n</div>\n\n<!--The actual tray button-->\n<button #button CresDigitalSend [Signal]=\"Signal\">\n  <div #circleContainer class=\"circle-container\">\n    <div class=\"circle\">\n      <svg viewBox=\"0 0 15 15\">\n        <text x=\"1\" y=\"14\">{{Icon}}</text>\n      </svg>\n    </div>\n    <div *ngIf=\"showPopup\" [@enterAnimation] [ngClass]=\"buildFbClass()\" class=\"feedback-circle\">\n    </div>\n  </div>\n  <h6>{{Label}}</h6>\n</button>\n", styles: [":host{overflow:visible;position:relative;height:100%}.volume-popup{position:absolute;bottom:105%;z-index:15;height:60vh;width:100vw;display:flex;flex-direction:column;justify-content:center}.pointer-arrow-container{height:10px;width:100%}.pointer-arrow{height:100%;width:20px;display:flex;justify-content:center}.triangle{fill:var(--m3-component-primary-50);height:10px;width:20px}.volume-control-area{height:calc(100% - 10px);min-width:60px;margin:auto;border-radius:5px;background-color:var(--m3-component-primary-50);color:var(--m3-component-primary-contrast-50)}button{height:100%;width:inherit;background-color:transparent;border:none;padding:5px;color:var(--m3-component-primary-contrast);display:flex;flex-direction:column;justify-content:space-between;align-items:center}button h6{margin-top:4px;height:25px}.circle{position:absolute;height:100%;width:100%;border-radius:100%;z-index:1;margin:auto;border:1px solid var(--m3-component-primary-contrast);display:flex;justify-content:center;align-items:center}.feedback-circle{position:absolute;height:100%;width:100%;border-radius:100%}.circle-container{height:calc(100% - 25px);position:relative}.fb-primary{background-color:var(--m3-component-primary)}.fb-accent{background-color:var(--m3-component-accent)}.fb-warn{background-color:var(--m3-component-warn)}svg{height:60%;fill:var(--m3-component-primary-contrast)}text{font-family:\"Material Icons\",sans-serif}h6{text-transform:uppercase}\n"], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i2.CresDigitalSendDirective, selector: "[CresDigitalSend]", inputs: ["Signal", "PressHold"] }, { type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], animations: [
        trigger('enterAnimation', [
            transition(':enter', [
                style({ transform: 'translateY(10%) scale(.9,.9)', opacity: 0 }),
                animate('200ms ease-in-out', style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }))
            ]),
            transition(':leave', [
                style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }),
                animate('200ms ease-in-out', style({ transform: 'translateY(6%) scale(.9,.9)', opacity: 0 }))
            ])
        ])
    ] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayPopupButtonComponent, decorators: [{
            type: Component,
            args: [{ selector: 'm3-tray-popup-button', animations: [
                        trigger('enterAnimation', [
                            transition(':enter', [
                                style({ transform: 'translateY(10%) scale(.9,.9)', opacity: 0 }),
                                animate('200ms ease-in-out', style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }))
                            ]),
                            transition(':leave', [
                                style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }),
                                animate('200ms ease-in-out', style({ transform: 'translateY(6%) scale(.9,.9)', opacity: 0 }))
                            ])
                        ])
                    ], template: "<!--Floating volume window that is drawn above the tray-->\n<!--Volume popup takes full viewport width width, used to center actual volume controls-->\n<div class=\"volume-popup\"\n    #volumePopup\n    [@enterAnimation]\n    *ngIf=\"showPopup\">\n\n    <!--Content container,sized based on input from ng-content-->\n    <div \n      #ControlArea \n      class=\"volume-control-area\"\n      (cdkObserveContent)=\"onNgContentChange($event)\">\n      <ng-content></ng-content>\n    </div>\n\n  <div class=\"pointer-arrow-container\">\n    <!--Inline SVG down arrow-->\n    <div #arrow class=\"pointer-arrow\">\n      <svg viewBox=\"0 0 100 50\" class=\"triangle\">\n\t\t    <polygon points=\"0,0 100,0 50,50\"/>\n\t    </svg>\n    </div>  \n  </div>\n</div>\n\n<!--The actual tray button-->\n<button #button CresDigitalSend [Signal]=\"Signal\">\n  <div #circleContainer class=\"circle-container\">\n    <div class=\"circle\">\n      <svg viewBox=\"0 0 15 15\">\n        <text x=\"1\" y=\"14\">{{Icon}}</text>\n      </svg>\n    </div>\n    <div *ngIf=\"showPopup\" [@enterAnimation] [ngClass]=\"buildFbClass()\" class=\"feedback-circle\">\n    </div>\n  </div>\n  <h6>{{Label}}</h6>\n</button>\n", styles: [":host{overflow:visible;position:relative;height:100%}.volume-popup{position:absolute;bottom:105%;z-index:15;height:60vh;width:100vw;display:flex;flex-direction:column;justify-content:center}.pointer-arrow-container{height:10px;width:100%}.pointer-arrow{height:100%;width:20px;display:flex;justify-content:center}.triangle{fill:var(--m3-component-primary-50);height:10px;width:20px}.volume-control-area{height:calc(100% - 10px);min-width:60px;margin:auto;border-radius:5px;background-color:var(--m3-component-primary-50);color:var(--m3-component-primary-contrast-50)}button{height:100%;width:inherit;background-color:transparent;border:none;padding:5px;color:var(--m3-component-primary-contrast);display:flex;flex-direction:column;justify-content:space-between;align-items:center}button h6{margin-top:4px;height:25px}.circle{position:absolute;height:100%;width:100%;border-radius:100%;z-index:1;margin:auto;border:1px solid var(--m3-component-primary-contrast);display:flex;justify-content:center;align-items:center}.feedback-circle{position:absolute;height:100%;width:100%;border-radius:100%}.circle-container{height:calc(100% - 25px);position:relative}.fb-primary{background-color:var(--m3-component-primary)}.fb-accent{background-color:var(--m3-component-accent)}.fb-warn{background-color:var(--m3-component-warn)}svg{height:60%;fill:var(--m3-component-primary-contrast)}text{font-family:\"Material Icons\",sans-serif}h6{text-transform:uppercase}\n"] }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }, { type: i0.ChangeDetectorRef }, { type: i0.NgZone }]; }, propDecorators: { Icon: [{
                type: Input
            }], Label: [{
                type: Input
            }], Signal: [{
                type: Input
            }], FBSignal: [{
                type: Input
            }], FbClass: [{
                type: Input
            }], arrowReff: [{
                type: ViewChild,
                args: ['arrow']
            }], controlArea: [{
                type: ViewChild,
                args: ['ControlArea']
            }], volumePopup: [{
                type: ViewChild,
                args: ['volumePopup']
            }], circleContainer: [{
                type: ViewChild,
                args: ['circleContainer']
            }], ButtonElement: [{
                type: ViewChild,
                args: ['button']
            }], onResize: [{
                type: HostListener,
                args: ['window:resize', ['$event']]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJheS1wb3B1cC1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbTMtY29tcG9uZW50cy9zcmMvbGliL3RyYXktcG9wdXAtYnV0dG9uL3RyYXktcG9wdXAtYnV0dG9uLmNvbXBvbmVudC50cyIsIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL20zLWNvbXBvbmVudHMvc3JjL2xpYi90cmF5LXBvcHVwLWJ1dHRvbi90cmF5LXBvcHVwLWJ1dHRvbi5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBc0IsWUFBWSxFQUFFLFNBQVMsRUFBa0UsTUFBTSxlQUFlLENBQUM7QUFDOUosT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDdEQsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRTFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTs7OztBQXFCckMsTUFBTSxPQUFPLDBCQUEwQjtJQUVyQyxZQUNZLFdBQXNCLEVBQ3RCLFFBQWtCLEVBQ2xCLGVBQWlDLEVBQ2pDLElBQVc7UUFIWCxnQkFBVyxHQUFYLFdBQVcsQ0FBVztRQUN0QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLG9CQUFlLEdBQWYsZUFBZSxDQUFrQjtRQUNqQyxTQUFJLEdBQUosSUFBSSxDQUFPO1FBT2QsU0FBSSxHQUFVLEVBQUUsQ0FBQztRQUNqQixVQUFLLEdBQVUsRUFBRSxDQUFBO1FBQ2pCLFdBQU0sR0FBVSxFQUFFLENBQUE7UUFDbEIsYUFBUSxHQUFVLEVBQUUsQ0FBQztRQUNyQixZQUFPLEdBQTJDLFNBQVMsQ0FBQztRQVNyRSxtQkFBYyxHQUFVLENBQUMsQ0FBQztRQUMxQixnQkFBVyxHQUFVLENBQUMsQ0FBQztRQUN2QixzQkFBaUIsR0FBVyxDQUFDLENBQUM7UUFDOUIsMkJBQTJCO1FBQzNCLHFCQUFnQixHQUFVLENBQUMsQ0FBQztRQUM1QixnQkFBVyxHQUFVLENBQUMsQ0FBQztRQUN2QixjQUFTLEdBQVcsS0FBSyxDQUFDO0lBekJ6QixDQUFDO0lBMEJGLDRDQUE0QztJQUU1QyxRQUFRLENBQUMsS0FBVTtRQUNqQix5Q0FBeUM7UUFDekMseUNBQXlDO1FBQ3pDLElBQUcsSUFBSSxDQUFDLFNBQVM7WUFDZixJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQTtJQUNuQyxDQUFDO0lBRUQscUNBQXFDO0lBQ3JDLGlCQUFpQixDQUFDLEtBQVU7UUFDMUIsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssSUFBSSxDQUFDLFdBQVcsRUFBRSxhQUFhLENBQUMsV0FBVztZQUN0RSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDLCtCQUErQjtRQUVyRSxpRkFBaUY7UUFDakYsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUVyQywrREFBK0Q7UUFDL0QsSUFBSSxDQUFDLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUN6QyxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNkNBQTZDO0lBQzdDLHFCQUFxQixDQUFDLEtBQWE7UUFDakMsZ0RBQWdEO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFckMsdUNBQXVDO1FBQ3ZDLElBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNqQixzRUFBc0U7WUFDdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQzVDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FDUixDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2YsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7WUFDbEMsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsMkVBQTJFO1lBQzNFLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQztTQUMzQjtJQUNILENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLE1BQU0sRUFBRSxXQUFXLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsaURBQWlEO0lBQ2pELHdCQUF3QjtRQUN0QixvREFBb0Q7UUFDcEQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsYUFBYSxDQUFDLENBQUE7UUFDN0UsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxhQUFhLENBQUMsV0FBVyxDQUFDO1FBQ2hFLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsYUFBYSxDQUFDLFdBQVcsQ0FBQztRQUMvRCxJQUFJLHFCQUFxQixHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsYUFBYSxDQUFDLFdBQVcsQ0FBQztRQUV4RSxJQUFJLFdBQVcsR0FBVSxDQUFDLENBQUM7UUFDM0IsV0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLFdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEVBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUM7UUFDNUUsV0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLDRCQUE0QixDQUFDLFdBQVcsRUFBQyxXQUFXLEVBQUMsWUFBWSxFQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUM7UUFFckgsSUFBRyxXQUFXLEdBQUcsQ0FBQyxFQUFDO1lBQ2pCLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDdEM7SUFDSCxDQUFDO0lBRUQscUZBQXFGO0lBQ3JGLHVCQUF1QixDQUFDLFlBQW1CO1FBRXpDLDBDQUEwQztRQUMxQyxJQUFJLFdBQVcsR0FBRyxZQUFZLEdBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBRyxJQUFJLENBQUMsY0FBYyxLQUFLLFdBQVcsRUFBRztZQUN2QyxJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQztZQUVsQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBWSxDQUFDLGFBQWEsRUFBQyxhQUFhLEVBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxJQUFJLENBQUMsQ0FBQztZQUNqRyxPQUFPLElBQUksQ0FBQztTQUNiO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsNEZBQTRGO0lBQzVGLHNCQUFzQixDQUFDLFlBQW1CLEVBQUUsWUFBbUI7UUFFN0QsSUFBSSxVQUFVLEdBQVUsRUFBRSxDQUFDO1FBQzNCLHlGQUF5RjtRQUN6RixJQUFJLGNBQWMsR0FBRyxZQUFZLEdBQUcsQ0FBQyxZQUFZLEdBQUMsVUFBVSxDQUFDLEdBQUMsQ0FBQyxDQUFDO1FBQ2hFLElBQUcsSUFBSSxDQUFDLFdBQVcsS0FBSyxjQUFjLEVBQUU7WUFDdEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxjQUFjLENBQUE7WUFDakMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxhQUFhLEVBQUMsYUFBYSxFQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsSUFBSSxDQUFDLENBQUM7WUFDNUYsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELHNGQUFzRjtJQUN0Riw0QkFBNEIsQ0FBQyxXQUFrQixFQUFFLFdBQWtCLEVBQUUsZ0JBQXVCLEVBQUUseUJBQWdDO1FBQzVILElBQUksb0JBQW9CLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFdBQVcsRUFBQyxXQUFXLEVBQUMsZ0JBQWdCLEVBQUMseUJBQXlCLENBQUMsQ0FBQztRQUUzSCxJQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxvQkFBb0IsRUFBRTtZQUNqRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0JBQW9CLENBQUM7WUFFOUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVksQ0FBQyxhQUFhLEVBQUMsYUFBYSxFQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsQ0FBQztZQUNwRyxPQUFPLElBQUksQ0FBQztTQUNiO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsNEZBQTRGO0lBQzVGLHNCQUFzQixDQUFDLFdBQWtCLEVBQUUsV0FBa0IsRUFBRSxnQkFBdUIsRUFBRSx5QkFBZ0M7UUFDdEgsSUFBSSxZQUFZLEdBQUcsV0FBVyxHQUFHLENBQUMsV0FBVyxHQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pELElBQUksZ0JBQWdCLEdBQUcsZ0JBQWdCLEdBQUMsQ0FBQyxDQUFDO1FBRTFDLHdDQUF3QztRQUN4QyxJQUNFLFlBQVksR0FBRyxnQkFBZ0IsR0FBRyxFQUFFO1lBQ3BDLENBQUMseUJBQXlCLEdBQUMsWUFBWSxDQUFDLEdBQUMsZ0JBQWdCLEdBQUUsRUFBRSxFQUFDO1lBQzVELE9BQU8sWUFBWSxHQUFHLGdCQUFnQixDQUFDO1NBQzFDO1FBRUQsMERBQTBEO1FBQzFELCtEQUErRDtRQUMvRCxJQUFHLFlBQVksR0FBRyxDQUFDLHlCQUF5QixHQUFDLENBQUMsQ0FBQyxFQUFFO1lBQy9DLE9BQU8seUJBQXlCLEdBQUMsZ0JBQWdCLEdBQUMsRUFBRSxDQUFDO1NBQ3REO1FBRUQsaURBQWlEO1FBQ2pELE9BQU8sRUFBRSxDQUFBO0lBQ1gsQ0FBQztJQUVELG9CQUFvQjtRQUNsQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFDaEUsSUFBRyxJQUFJLENBQUMsV0FBVyxLQUFLLFFBQVEsRUFBRTtZQUNoQyxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQztZQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLGFBQWEsRUFBQyxPQUFPLEVBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxJQUFJLENBQUMsQ0FBQztZQUU1RixPQUFPLElBQUksQ0FBQztTQUNiO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsWUFBWTtRQUNWLFFBQVEsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNwQixLQUFLLFNBQVM7Z0JBQ1YsT0FBTyxZQUFZLENBQUM7WUFDeEIsS0FBSyxRQUFRO2dCQUNULE9BQU8sV0FBVyxDQUFDO1lBQ3ZCLEtBQUssTUFBTTtnQkFDTCxPQUFPLFNBQVMsQ0FBQztZQUN2QjtnQkFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7U0FDdkI7SUFDSCxDQUFDO0lBRUQscUZBQXFGO0lBQ3JGLG1CQUFtQixDQUFDLE9BQVc7UUFDN0IsSUFBSSxRQUFRLEdBQVUsQ0FBQyxDQUFBO1FBRXZCLElBQUcsT0FBTyxDQUFDLFlBQVksS0FBSyxJQUFJLEVBQUU7WUFDaEMsUUFBUSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDM0Q7UUFDRCxRQUFRLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQztRQUUvQixPQUFPLFFBQVEsQ0FBQztJQUNsQixDQUFDOzt1SEFoTlUsMEJBQTBCOzJHQUExQiwwQkFBMEIsNnJCQ3pCdkMsNHFDQXNDQSx1dEREOUJjO1FBQ1YsT0FBTyxDQUNMLGdCQUFnQixFQUFFO1lBQ2hCLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0JBQ25CLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSw4QkFBOEIsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUM7Z0JBQzlELE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsMEJBQTBCLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7YUFDekYsQ0FBQztZQUNGLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0JBQ25CLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSwwQkFBMEIsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUM7Z0JBQzFELE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsNkJBQTZCLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7YUFDNUYsQ0FBQztTQUNILENBQ0Y7S0FDRjsyRkFJVSwwQkFBMEI7a0JBbkJ0QyxTQUFTOytCQUNFLHNCQUFzQixjQUNwQjt3QkFDVixPQUFPLENBQ0wsZ0JBQWdCLEVBQUU7NEJBQ2hCLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0NBQ25CLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSw4QkFBOEIsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUM7Z0NBQzlELE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsMEJBQTBCLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7NkJBQ3pGLENBQUM7NEJBQ0YsVUFBVSxDQUFDLFFBQVEsRUFBRTtnQ0FDbkIsS0FBSyxDQUFDLEVBQUMsU0FBUyxFQUFFLDBCQUEwQixFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQztnQ0FDMUQsT0FBTyxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSw2QkFBNkIsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQzs2QkFDNUYsQ0FBQzt5QkFDSCxDQUNGO3FCQUNGOzhLQWlCUSxJQUFJO3NCQUFaLEtBQUs7Z0JBQ0csS0FBSztzQkFBYixLQUFLO2dCQUNHLE1BQU07c0JBQWQsS0FBSztnQkFDRyxRQUFRO3NCQUFoQixLQUFLO2dCQUNHLE9BQU87c0JBQWYsS0FBSztnQkFHYyxTQUFTO3NCQUE1QixTQUFTO3VCQUFDLE9BQU87Z0JBQ1EsV0FBVztzQkFBcEMsU0FBUzt1QkFBQyxhQUFhO2dCQUNFLFdBQVc7c0JBQXBDLFNBQVM7dUJBQUMsYUFBYTtnQkFDTSxlQUFlO3NCQUE1QyxTQUFTO3VCQUFDLGlCQUFpQjtnQkFDUCxhQUFhO3NCQUFqQyxTQUFTO3VCQUFDLFFBQVE7Z0JBV25CLFFBQVE7c0JBRFAsWUFBWTt1QkFBQyxlQUFlLEVBQUMsQ0FBQyxRQUFRLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIEVsZW1lbnRSZWYsIEhvc3RMaXN0ZW5lciwgVmlld0NoaWxkLCBPbkRlc3Ryb3ksIEFmdGVyVmlld0luaXQsIFJlbmRlcmVyMiwgQ2hhbmdlRGV0ZWN0b3JSZWYsIE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGlnaXRhbFNpZ09ic2VydmVyIH0gZnJvbSAnY3Jlcy1jb20td3JhcHBlcic7XG5pbXBvcnQgeyB0cmlnZ2VyLCBzdHlsZSwgYW5pbWF0ZSwgdHJhbnNpdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YWtlIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ20zLXRyYXktcG9wdXAtYnV0dG9uJyxcbiAgYW5pbWF0aW9uczogW1xuICAgIHRyaWdnZXIoXG4gICAgICAnZW50ZXJBbmltYXRpb24nLCBbXG4gICAgICAgIHRyYW5zaXRpb24oJzplbnRlcicsIFtcbiAgICAgICAgICBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWSgxMCUpIHNjYWxlKC45LC45KScsIG9wYWNpdHk6IDB9KSxcbiAgICAgICAgICBhbmltYXRlKCcyMDBtcyBlYXNlLWluLW91dCcsIHN0eWxlKHt0cmFuc2Zvcm06ICd0cmFuc2xhdGVZKDApIHNjYWxlKDEsMSknLCBvcGFjaXR5OiAxfSkpXG4gICAgICAgIF0pLFxuICAgICAgICB0cmFuc2l0aW9uKCc6bGVhdmUnLCBbXG4gICAgICAgICAgc3R5bGUoe3RyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMCkgc2NhbGUoMSwxKScsIG9wYWNpdHk6IDF9KSxcbiAgICAgICAgICBhbmltYXRlKCcyMDBtcyBlYXNlLWluLW91dCcsIHN0eWxlKHt0cmFuc2Zvcm06ICd0cmFuc2xhdGVZKDYlKSBzY2FsZSguOSwuOSknLCBvcGFjaXR5OiAwfSkpXG4gICAgICAgIF0pXG4gICAgICBdXG4gICAgKVxuICBdLFxuICB0ZW1wbGF0ZVVybDogJy4vdHJheS1wb3B1cC1idXR0b24uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9fdHJheS1wb3B1cC1idXR0b24uY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBNM1RyYXlQb3B1cEJ1dHRvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95IHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICAgIHByaXZhdGUgcm9vdEVsZW1lbnQ6RWxlbWVudFJlZixcbiAgICAgIHByaXZhdGUgcmVuZGVyZXI6UmVuZGVyZXIyLFxuICAgICAgcHJpdmF0ZSBjaGFuZ2VEZXRlY3Rpb246Q2hhbmdlRGV0ZWN0b3JSZWYsXG4gICAgICBwcml2YXRlIHpvbmU6Tmdab25lICkge1xuICAgfVxuICBcbiAgLy9vYnNlcnZhYmxlIGRpZ2l0YWwgc2lnbmFsIGZyb20gQ29udHJvbCBzeXN0ZW1cbiAgYnRuRmI/Ok9ic2VydmFibGU8Ym9vbGVhbj47XG4gIGJ0blN1Yj86U3Vic2NyaXB0aW9uO1xuXG4gIEBJbnB1dCgpIEljb246c3RyaW5nID0gXCJcIjtcbiAgQElucHV0KCkgTGFiZWw6c3RyaW5nID0gXCJcIlxuICBASW5wdXQoKSBTaWduYWw6c3RyaW5nID0gXCJcIlxuICBASW5wdXQoKSBGQlNpZ25hbDpzdHJpbmcgPSBcIlwiO1xuICBASW5wdXQoKSBGYkNsYXNzOiAncHJpbWFyeScgfCAnYWNjZW50JyB8ICd3YXJuJyB8IHN0cmluZyA9ICdwcmltYXJ5JztcblxuICAvL2dldCBjb250ZW50IGFuZCBhcnJvdyBkaW1lbnRpb25zXG4gIEBWaWV3Q2hpbGQoJ2Fycm93JykgYXJyb3dSZWZmPzpFbGVtZW50UmVmO1xuICBAVmlld0NoaWxkKCdDb250cm9sQXJlYScpIGNvbnRyb2xBcmVhPzpFbGVtZW50UmVmO1xuICBAVmlld0NoaWxkKCd2b2x1bWVQb3B1cCcpIHZvbHVtZVBvcHVwPzpFbGVtZW50UmVmO1xuICBAVmlld0NoaWxkKCdjaXJjbGVDb250YWluZXInKSBjaXJjbGVDb250YWluZXI/OkVsZW1lbnRSZWY7XG4gIEBWaWV3Q2hpbGQoJ2J1dHRvbicpIEJ1dHRvbkVsZW1lbnQ/OkVsZW1lbnRSZWY7XG5cbiAgd2luZG93TG9jYXRpb246bnVtYmVyID0gMDtcbiAgYXJyb3dPZmZzZXQ6bnVtYmVyID0gMDtcbiAgY29udHJvbEFyZWFPZmZzZXQ6IG51bWJlciA9IDA7XG4gIC8vdmlld3BvcnRXaWR0aDpudW1iZXIgPSAwO1xuICBjb250cm9sQXJlYVdpZHRoOm51bWJlciA9IDA7XG4gIGNpcmNsZVdpZHRoOm51bWJlciA9IDA7XG4gIHNob3dQb3B1cDpib29sZWFuID0gZmFsc2U7XG4gIC8vR2V0IHRoZSBsb2NhdGlvbiBmb3IgdGhlIGZ1bGwgdm9sdW1lIHBvcHVwXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpyZXNpemUnLFsnJGV2ZW50J10pXG4gIG9uUmVzaXplKGV2ZW50PzphbnkpIHtcbiAgICAvL3RoaXMudmlld3BvcnRXaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoO1xuICAgIC8vb25seSB1cGRhdGV3IHRoZSBwb3B1cCBpZiBpdCBpcyB2aXNhYmxlXG4gICAgaWYodGhpcy5zaG93UG9wdXApXG4gICAgICB0aGlzLnJlYnVpbGRQb3NpdGlvbkxvY2F0aW9ucygpXG4gIH1cblxuICAvL3VzZWQgdG8gZGV0ZWN0IGNoYW5nZXMgaW4gTmdDb250ZW50XG4gIG9uTmdDb250ZW50Q2hhbmdlKGV2ZW50PzphbnkpOnZvaWQge1xuICAgIGlmKHRoaXMuY29udHJvbEFyZWFXaWR0aCAhPT0gdGhpcy5jb250cm9sQXJlYT8ubmF0aXZlRWxlbWVudC5vZmZzZXRXaWR0aClcbiAgICAgIHRoaXMub25SZXNpemUoKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuY2hhbmdlRGV0ZWN0aW9uLmRldGFjaCgpO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCk6dm9pZCB7XG4gICAgdGhpcy5jaGFuZ2VEZXRlY3Rpb24uZGV0ZWN0Q2hhbmdlcygpOyAvL2luaXRpYWwgY2hhbmdlIGRldGVjdGlvbiBwYXNzXG4gICAgXG4gICAgLy9zZXQgdGhlIHdpZHRoIG9mIHRoZSBjaXJjbGUgYXMgd2UgY2FuJ3QgdXNlIHRoZSBhc3BlY3QtcmF0aW8gY3NzIGNsYXNzIG9uIGEgVFNXXG4gICAgdGhpcy5jYWxjdWxhdGVDaXJjbGVXaWR0aCgpO1xuICAgIHRoaXMuY2hhbmdlRGV0ZWN0aW9uLmRldGVjdENoYW5nZXMoKTtcbiAgICBcbiAgICAvL3N1YnNjcmliZSB0byB0aGUgQ1Mgc2lnbmFsIGFuZCBwYXNzIGl0IHRvIHRoZSB1cGRhdGUgZnVuY3Rpb25cbiAgICB0aGlzLmJ0bkZiID0gRGlnaXRhbFNpZ09ic2VydmVyLk9ic2VydmUodGhpcy5GQlNpZ25hbCk7XG4gICAgdGhpcy5idG5TdWIgPSB0aGlzLmJ0bkZiLnN1YnNjcmliZSgodmFsKSA9PiB7XG4gICAgICB0aGlzLlVwZGF0ZVBvcHVwVmlzYWJpbGl0eSh2YWwpO1xuICAgIH0pO1xuICB9XG5cbiAgLy9tYW5hZ2VzIHRoZSBoaWRpbmcgYW5kIHNob3dpbmcgb2YgdGhlIHBvcHVwXG4gIFVwZGF0ZVBvcHVwVmlzYWJpbGl0eShzdGF0ZTpib29sZWFuKSB7XG4gICAgLy91cGRhdGUgdGhlIHBvcHVwIGFuZCB0aGVuIHJ1biBjaGFuZ2UgZGV0ZWN0aW9uXG4gICAgdGhpcy5zaG93UG9wdXAgPSBzdGF0ZTtcbiAgICB0aGlzLmNoYW5nZURldGVjdGlvbi5kZXRlY3RDaGFuZ2VzKCk7XG4gICAgXG4gICAgLy90aGVuIHdlIG5lZWQgdG8gZmlndXJlIG91dCB0aGlzIHBvcHVwXG4gICAgaWYodGhpcy5zaG93UG9wdXApIHtcbiAgICAgIC8vd2FpdCB1bnRpbCB0aGUgem9uZSBoYXMgc2V0dGVsZWQgYmVmb3JlIHN0YXJ0aW5nIHRoZSBwb3NpdGlvbiB1cGRhdGVcbiAgICAgIHRoaXMuem9uZS5vbk1pY3JvdGFza0VtcHR5LmFzT2JzZXJ2YWJsZSgpLnBpcGUoXG4gICAgICAgIHRha2UoMSlcbiAgICAgICkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgdGhpcy5yZWJ1aWxkUG9zaXRpb25Mb2NhdGlvbnMoKTtcbiAgICAgIH0pOyAgICAgIFxuICAgIH0gZWxzZSB7XG4gICAgICAvL3plcm8gb3V0IHRoZXNlIHZhbHVlcyBzbyB0aGV5IGFyZSByZWNhbGN1bGF0ZWQgb24gdGhlIG5leHQgd2luZG93IHNob3dpbmdcbiAgICAgIHRoaXMud2luZG93TG9jYXRpb24gPSAwO1xuICAgICAgdGhpcy5hcnJvd09mZnNldCA9IDA7XG4gICAgICB0aGlzLmNvbnRyb2xBcmVhT2Zmc2V0ID0gMDtcbiAgICAgIHRoaXMuY29udHJvbEFyZWFXaWR0aCA9IDA7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gICAgdGhpcy5idG5TdWI/LnVuc3Vic2NyaWJlKCk7XG4gIH1cblxuICAvL0J1aWxkIGxvY2F0aW9ucyBmb3IgdGhlIHBvcHVwIHdpbmRvdyBhbmQgYXJyb3cuXG4gIHJlYnVpbGRQb3NpdGlvbkxvY2F0aW9ucygpIHtcbiAgICAvL0dldCBidXR0b24gbG9jYXRpb24sIHdpZHRoIGFuZCB2aWV3cG9ydCBwYXJhbWV0ZXJzXG4gICAgbGV0IGJ1dHRvblN0YXJ0ID0gdGhpcy5HZXRFbGVtZW50WFBvc2l0aW9uKHRoaXMuQnV0dG9uRWxlbWVudD8ubmF0aXZlRWxlbWVudClcbiAgICBsZXQgYnV0dG9uV2lkdGggPSB0aGlzLkJ1dHRvbkVsZW1lbnQ/Lm5hdGl2ZUVsZW1lbnQub2Zmc2V0V2lkdGg7XG4gICAgbGV0IGNvbnRyb2xXaWR0aCA9IHRoaXMuY29udHJvbEFyZWE/Lm5hdGl2ZUVsZW1lbnQub2Zmc2V0V2lkdGg7XG4gICAgbGV0IGNvbnRyb2xDb250YWluZXJXaWR0aCA9IHRoaXMudm9sdW1lUG9wdXA/Lm5hdGl2ZUVsZW1lbnQub2Zmc2V0V2lkdGg7XG4gICAgXG4gICAgbGV0IGNoYW5nZUNvdW50Om51bWJlciA9IDA7XG4gICAgY2hhbmdlQ291bnQgKz0gKHRoaXMuY2FsY3VsYXRlV2luZG93TG9jYXRpb24oYnV0dG9uU3RhcnQpID8gMTowKTtcbiAgICBjaGFuZ2VDb3VudCArPSAodGhpcy5jYWxjdWxhdGVBcnJvd0xvYWN0aW9uKGJ1dHRvblN0YXJ0LGJ1dHRvbldpZHRoKSA/IDE6MCk7XG4gICAgY2hhbmdlQ291bnQgKz0gKHRoaXMuY2FsY3VsYXRlQ29udHJvbEFyZWFQb3NpdGlvbihidXR0b25TdGFydCxidXR0b25XaWR0aCxjb250cm9sV2lkdGgsY29udHJvbENvbnRhaW5lcldpZHRoKSA/IDE6MCk7XG5cbiAgICBpZihjaGFuZ2VDb3VudCA+IDApe1xuICAgICAgdGhpcy5jaGFuZ2VEZXRlY3Rpb24uZGV0ZWN0Q2hhbmdlcygpO1xuICAgIH1cbiAgfVxuXG4gIC8vZ2V0cyB0aGUgd2luZG93IGxvY2F0aW9uLCBpZiBpdCBoYXMgY2hhbmdlZCwgcmV0dXJuIHRydWUgZm9yIGNoYW5nIGRldGVjdGlvbiB0byBydW5cbiAgY2FsY3VsYXRlV2luZG93TG9jYXRpb24oZWxlbWVudFN0YXJ0Om51bWJlcik6Ym9vbGVhbiB7XG5cbiAgICAvL1dpbmRvdyBMb2FjYXRpb24gaXMganVzdCAtMSplbGVtZW50U3RhcnRcbiAgICBsZXQgbmV3TG9jYXRpb24gPSBlbGVtZW50U3RhcnQqLTE7XG4gICAgaWYodGhpcy53aW5kb3dMb2NhdGlvbiAhPT0gbmV3TG9jYXRpb24pICB7XG4gICAgICB0aGlzLndpbmRvd0xvY2F0aW9uID0gbmV3TG9jYXRpb247XG5cbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy52b2x1bWVQb3B1cCEubmF0aXZlRWxlbWVudCwnbWFyZ2luLWxlZnQnLGAke3RoaXMud2luZG93TG9jYXRpb259cHhgKTtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICAvL0FuYWx5c2UgYW5kIGNhbGN1bGF0ZSB3aGVyZSB0aGUgYXJyb3cgc2hvdWxkIGdvLCByZXR1cm5zIHRydWUgaWYgYSBjaGFuZ2UgbmVlZHMgdG8gYmUgbWFkZVxuICBjYWxjdWxhdGVBcnJvd0xvYWN0aW9uKGVsZW1lbnRTdGFydDpudW1iZXIsIGVsZW1lbnRXaWR0aDpudW1iZXIpOmJvb2xlYW4ge1xuXG4gICAgbGV0IGFycm93V2lkdGg6bnVtYmVyID0gMjA7XG4gICAgLy9BcnJvdyBPZmZzZXQgaXMgZWxlbWVudFN0YXJ0ICsgMS8yIHRoZSB3aXRoIG9mIHRoZSBidXR0b24gLSAxLzIgdGhlIGxlZ250aCBvZiB0aGUgYXJyb3dcbiAgICBsZXQgbmV3QXJyb3dPZmZzZXQgPSBlbGVtZW50U3RhcnQgKyAoZWxlbWVudFdpZHRoLWFycm93V2lkdGgpLzI7XG4gICAgaWYodGhpcy5hcnJvd09mZnNldCAhPT0gbmV3QXJyb3dPZmZzZXQpIHtcbiAgICAgIHRoaXMuYXJyb3dPZmZzZXQgPSBuZXdBcnJvd09mZnNldFxuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmFycm93UmVmZj8ubmF0aXZlRWxlbWVudCxcIm1hcmdpbi1sZWZ0XCIsYCR7dGhpcy5hcnJvd09mZnNldH1weGApO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8vQW5hbHlzZSB0aGUgcG9zaXRpb24gb2YgdGhlIGNvbnRyb2wgYXJlYSwgaWYgYSBjaGFuZ2UgbmVlZHMgdG8gYmUgbWFkZSwgcmV0dXJucyB0cnVlXG4gIGNhbGN1bGF0ZUNvbnRyb2xBcmVhUG9zaXRpb24oYnV0dG9uU3RhcnQ6bnVtYmVyLCBidXR0b25XaWR0aDpudW1iZXIsIGNvbnRyb2xBcmVhV2lkdGg6bnVtYmVyLCBjb250cm9sQXJlYUNvbnRhaW5lcldpZHRoOm51bWJlcik6Ym9vbGVhbiB7XG4gICAgbGV0IG5ld0NvbnRyb2xBcmVhT2Zmc2V0ID0gdGhpcy5nZXRDb250cm9sQXJlYVBvc2l0aW9uKGJ1dHRvblN0YXJ0LGJ1dHRvbldpZHRoLGNvbnRyb2xBcmVhV2lkdGgsY29udHJvbEFyZWFDb250YWluZXJXaWR0aCk7XG5cbiAgICBpZih0aGlzLmNvbnRyb2xBcmVhT2Zmc2V0ICE9IG5ld0NvbnRyb2xBcmVhT2Zmc2V0KSB7XG4gICAgICB0aGlzLmNvbnRyb2xBcmVhT2Zmc2V0ID0gbmV3Q29udHJvbEFyZWFPZmZzZXQ7XG4gICAgICBcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5jb250cm9sQXJlYSEubmF0aXZlRWxlbWVudCxcIm1hcmdpbi1sZWZ0XCIsYCR7dGhpcy5jb250cm9sQXJlYU9mZnNldH1weGApO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8vc29sdmUgZm9yIHdoZXJlIHRoZSB3aW5kb3cgbmVlZHMgdG8gZ28sIHJldHVybnMgYSBweCBmcm9tIHRoZSBsZWZ0IHdoZXJlIHRoZSB3aW5kb3cgc3RhcnRzXG4gIGdldENvbnRyb2xBcmVhUG9zaXRpb24oYnV0dG9uU3RhcnQ6bnVtYmVyLCBidXR0b25XaWR0aDpudW1iZXIsIGNvbnRyb2xBcmVhV2lkdGg6bnVtYmVyLCBjb250cm9sQXJlYUNvbnRhaW5lcldpZHRoOm51bWJlcik6bnVtYmVyIHtcbiAgICBsZXQgYnV0dG9uQ2VudGVyID0gYnV0dG9uU3RhcnQgKyAoYnV0dG9uV2lkdGgvMik7XG4gICAgbGV0IGhhbGZDb250cm9sV2lkdGggPSBjb250cm9sQXJlYVdpZHRoLzI7XG5cbiAgICAvL2lmIGl0IGNhbiBiZSBjZW50ZXJlZCwganVzdCBkbyB0aGF0Li4uXG4gICAgaWYoXG4gICAgICBidXR0b25DZW50ZXIgLSBoYWxmQ29udHJvbFdpZHRoID4gMzAgJiYgXG4gICAgICAoY29udHJvbEFyZWFDb250YWluZXJXaWR0aC1idXR0b25DZW50ZXIpLWhhbGZDb250cm9sV2lkdGggPjMwKXtcbiAgICAgICAgcmV0dXJuIGJ1dHRvbkNlbnRlciAtIGhhbGZDb250cm9sV2lkdGg7XG4gICAgfVxuXG4gICAgLy9pZiBpdCBjYW4ndCBiZSBjZW50ZXJlZCwgd2UgcGluIHRvIHRoZSBsZWZ0IG9yIHRoZSByaWdodFxuICAgIC8vYnV0dG9uIGlzIGNsb3NlciB0byB0aGUgcmlnaHQsIHNvIHBpbiBpdCAzMCBweCBmb3JtIHRoZSByaWdodFxuICAgIGlmKGJ1dHRvbkNlbnRlciA+IChjb250cm9sQXJlYUNvbnRhaW5lcldpZHRoLzIpKSB7XG4gICAgICByZXR1cm4gY29udHJvbEFyZWFDb250YWluZXJXaWR0aC1jb250cm9sQXJlYVdpZHRoLTMwO1xuICAgIH1cblxuICAgIC8vaXRzIGNsb3NlciB0byB0aGUgbGVmdCBzbyBwaW4gMzAgcHggdG8gdGhlIGxlZnRcbiAgICByZXR1cm4gMzBcbiAgfVxuXG4gIGNhbGN1bGF0ZUNpcmNsZVdpZHRoKCk6IGJvb2xlYW4ge1xuICAgIGxldCBuZXdXaWR0aCA9IHRoaXMuY2lyY2xlQ29udGFpbmVyPy5uYXRpdmVFbGVtZW50Lm9mZnNldEhlaWdodDtcbiAgICBpZih0aGlzLmNpcmNsZVdpZHRoICE9PSBuZXdXaWR0aCkge1xuICAgICAgdGhpcy5jaXJjbGVXaWR0aCA9IG5ld1dpZHRoO1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNpcmNsZUNvbnRhaW5lcj8ubmF0aXZlRWxlbWVudCwnd2lkdGgnLGAke3RoaXMuY2lyY2xlV2lkdGh9cHhgKTtcbiAgICAgIFxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGJ1aWxkRmJDbGFzcygpIHtcbiAgICBzd2l0Y2ggKHRoaXMuRmJDbGFzcykge1xuICAgICAgY2FzZSAncHJpbWFyeSc6XG4gICAgICAgICAgcmV0dXJuICdmYi1wcmltYXJ5JztcbiAgICAgIGNhc2UgJ2FjY2VudCc6XG4gICAgICAgICAgcmV0dXJuICdmYi1hY2NlbnQnO1xuICAgICAgY2FzZSAnd2Fybic6XG4gICAgICAgICAgICByZXR1cm4gJ2ZiLXdhcm4nO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIHRoaXMuRmJDbGFzcztcbiAgICB9XG4gIH1cblxuICAvL2Z1bmN0aW9uIHRvIHJlY3Vyc2l2bHkgZ2V0IHRoZSBvZmZzZXRMZWZ0IG9mIGFsbCBwYXJlbnQgY29udGFpbmVycywgcmV0dXJucyB0aGUgc3VtXG4gIEdldEVsZW1lbnRYUG9zaXRpb24oZWxlbWVudDphbnkpOm51bWJlciB7XG4gICAgbGV0IHBvc2l0aW9uOm51bWJlciA9IDBcblxuICAgIGlmKGVsZW1lbnQub2Zmc2V0UGFyZW50ICE9PSBudWxsKSB7XG4gICAgICBwb3NpdGlvbiA9IHRoaXMuR2V0RWxlbWVudFhQb3NpdGlvbihlbGVtZW50Lm9mZnNldFBhcmVudCk7XG4gICAgfVxuICAgIHBvc2l0aW9uICs9IGVsZW1lbnQub2Zmc2V0TGVmdDtcblxuICAgIHJldHVybiBwb3NpdGlvbjtcbiAgfVxuXG59XG4iLCI8IS0tRmxvYXRpbmcgdm9sdW1lIHdpbmRvdyB0aGF0IGlzIGRyYXduIGFib3ZlIHRoZSB0cmF5LS0+XG48IS0tVm9sdW1lIHBvcHVwIHRha2VzIGZ1bGwgdmlld3BvcnQgd2lkdGggd2lkdGgsIHVzZWQgdG8gY2VudGVyIGFjdHVhbCB2b2x1bWUgY29udHJvbHMtLT5cbjxkaXYgY2xhc3M9XCJ2b2x1bWUtcG9wdXBcIlxuICAgICN2b2x1bWVQb3B1cFxuICAgIFtAZW50ZXJBbmltYXRpb25dXG4gICAgKm5nSWY9XCJzaG93UG9wdXBcIj5cblxuICAgIDwhLS1Db250ZW50IGNvbnRhaW5lcixzaXplZCBiYXNlZCBvbiBpbnB1dCBmcm9tIG5nLWNvbnRlbnQtLT5cbiAgICA8ZGl2IFxuICAgICAgI0NvbnRyb2xBcmVhIFxuICAgICAgY2xhc3M9XCJ2b2x1bWUtY29udHJvbC1hcmVhXCJcbiAgICAgIChjZGtPYnNlcnZlQ29udGVudCk9XCJvbk5nQ29udGVudENoYW5nZSgkZXZlbnQpXCI+XG4gICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XG4gICAgPC9kaXY+XG5cbiAgPGRpdiBjbGFzcz1cInBvaW50ZXItYXJyb3ctY29udGFpbmVyXCI+XG4gICAgPCEtLUlubGluZSBTVkcgZG93biBhcnJvdy0tPlxuICAgIDxkaXYgI2Fycm93IGNsYXNzPVwicG9pbnRlci1hcnJvd1wiPlxuICAgICAgPHN2ZyB2aWV3Qm94PVwiMCAwIDEwMCA1MFwiIGNsYXNzPVwidHJpYW5nbGVcIj5cblx0XHQgICAgPHBvbHlnb24gcG9pbnRzPVwiMCwwIDEwMCwwIDUwLDUwXCIvPlxuXHQgICAgPC9zdmc+XG4gICAgPC9kaXY+ICBcbiAgPC9kaXY+XG48L2Rpdj5cblxuPCEtLVRoZSBhY3R1YWwgdHJheSBidXR0b24tLT5cbjxidXR0b24gI2J1dHRvbiBDcmVzRGlnaXRhbFNlbmQgW1NpZ25hbF09XCJTaWduYWxcIj5cbiAgPGRpdiAjY2lyY2xlQ29udGFpbmVyIGNsYXNzPVwiY2lyY2xlLWNvbnRhaW5lclwiPlxuICAgIDxkaXYgY2xhc3M9XCJjaXJjbGVcIj5cbiAgICAgIDxzdmcgdmlld0JveD1cIjAgMCAxNSAxNVwiPlxuICAgICAgICA8dGV4dCB4PVwiMVwiIHk9XCIxNFwiPnt7SWNvbn19PC90ZXh0PlxuICAgICAgPC9zdmc+XG4gICAgPC9kaXY+XG4gICAgPGRpdiAqbmdJZj1cInNob3dQb3B1cFwiIFtAZW50ZXJBbmltYXRpb25dIFtuZ0NsYXNzXT1cImJ1aWxkRmJDbGFzcygpXCIgY2xhc3M9XCJmZWVkYmFjay1jaXJjbGVcIj5cbiAgICA8L2Rpdj5cbiAgPC9kaXY+XG4gIDxoNj57e0xhYmVsfX08L2g2PlxuPC9idXR0b24+XG4iXX0=