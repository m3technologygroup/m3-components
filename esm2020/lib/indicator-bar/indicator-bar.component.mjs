import { Component, Input, ViewChild } from '@angular/core';
import { AnalogSigObserver } from 'cres-com-wrapper';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class M3IndicatorBarComponent {
    constructor(renderer, changeDetection) {
        this.renderer = renderer;
        this.changeDetection = changeDetection;
        this.FBSignal = '';
        this.FBClass = 'primary';
        this.Direction = 'horizontal';
        this.MinValue = 0;
        this.MaxValue = 65535;
        this.lastBarValue = -1;
    }
    ngOnInit() {
        this.changeDetection.detach();
    }
    ngAfterViewInit() {
        this.changeDetection.detectChanges();
        this.barSubHandel = AnalogSigObserver
            .Observe(this.FBSignal)
            .subscribe((val) => {
            let newVal = this.scaleValue(val); //convert it to 0.0%-100.0%
            if (newVal !== this.lastBarValue) { //only run change detection on new values
                this.lastBarValue = newVal;
                let direction = this.Direction === 'horizontal' ? 'width' : 'height';
                this.renderer.setStyle(this.BarObj.nativeElement, direction, `${this.lastBarValue}%`);
                this.changeDetection.detectChanges();
            }
        });
    }
    ngOnDestroy() {
        this.barSubHandel?.unsubscribe();
    }
    //scale to 0.0-100.0%
    scaleValue(val) {
        let pct = (val - this.MinValue) / (this.MaxValue - this.MinValue);
        return Math.round(pct * 1000) / 10;
    }
}
M3IndicatorBarComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarComponent, deps: [{ token: i0.Renderer2 }, { token: i0.ChangeDetectorRef }], target: i0.ɵɵFactoryTarget.Component });
M3IndicatorBarComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: M3IndicatorBarComponent, selector: "m3-indicator-bar", inputs: { FBSignal: "FBSignal", FBClass: "FBClass", Direction: "Direction", MinValue: "MinValue", MaxValue: "MaxValue" }, viewQueries: [{ propertyName: "BarObj", first: true, predicate: ["barobj"], descendants: true }], ngImport: i0, template: "<div class=\"obj-bar-container\" [ngClass]=\"Direction\">\n  <div class=\"obj-bar-statics\" #barobj [ngClass]=\"FBClass\"></div>\n</div>\n", styles: [".obj-bar-container{height:100%;width:100%}.obj-bar-statics{height:100%}.vertical{display:flex;flex-direction:column;justify-content:flex-end}.horizontal{display:block}.primary{background-color:var(--m3-component-primary)}.accent{background-color:var(--m3-component-accent)}.warn{background-color:var(--m3-component-warn)}\n"], directives: [{ type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarComponent, decorators: [{
            type: Component,
            args: [{ selector: 'm3-indicator-bar', template: "<div class=\"obj-bar-container\" [ngClass]=\"Direction\">\n  <div class=\"obj-bar-statics\" #barobj [ngClass]=\"FBClass\"></div>\n</div>\n", styles: [".obj-bar-container{height:100%;width:100%}.obj-bar-statics{height:100%}.vertical{display:flex;flex-direction:column;justify-content:flex-end}.horizontal{display:block}.primary{background-color:var(--m3-component-primary)}.accent{background-color:var(--m3-component-accent)}.warn{background-color:var(--m3-component-warn)}\n"] }]
        }], ctorParameters: function () { return [{ type: i0.Renderer2 }, { type: i0.ChangeDetectorRef }]; }, propDecorators: { FBSignal: [{
                type: Input
            }], FBClass: [{
                type: Input
            }], Direction: [{
                type: Input
            }], MinValue: [{
                type: Input
            }], MaxValue: [{
                type: Input
            }], BarObj: [{
                type: ViewChild,
                args: ['barobj']
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kaWNhdG9yLWJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9tMy1jb21wb25lbnRzL3NyYy9saWIvaW5kaWNhdG9yLWJhci9pbmRpY2F0b3ItYmFyLmNvbXBvbmVudC50cyIsIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL20zLWNvbXBvbmVudHMvc3JjL2xpYi9pbmRpY2F0b3ItYmFyL2luZGljYXRvci1iYXIuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFxQixTQUFTLEVBQUUsS0FBSyxFQUFxQixTQUFTLEVBQXdDLE1BQU0sZUFBZSxDQUFDO0FBQ3hJLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtCQUFrQixDQUFDOzs7QUFRckQsTUFBTSxPQUFPLHVCQUF1QjtJQWFsQyxZQUFvQixRQUFrQixFQUFVLGVBQWtDO1FBQTlELGFBQVEsR0FBUixRQUFRLENBQVU7UUFBVSxvQkFBZSxHQUFmLGVBQWUsQ0FBbUI7UUFYekUsYUFBUSxHQUFVLEVBQUUsQ0FBQztRQUNyQixZQUFPLEdBQVUsU0FBUyxDQUFDO1FBQzNCLGNBQVMsR0FBOEIsWUFBWSxDQUFDO1FBQ3BELGFBQVEsR0FBVyxDQUFDLENBQUM7UUFDckIsYUFBUSxHQUFXLEtBQUssQ0FBQztRQUtsQyxpQkFBWSxHQUFVLENBQUMsQ0FBQyxDQUFDO0lBRTZELENBQUM7SUFFdkYsUUFBUTtRQUNOLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUM7SUFHaEMsQ0FBQztJQUVELGVBQWU7UUFDYixJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRXJDLElBQUksQ0FBQyxZQUFZLEdBQUcsaUJBQWlCO2FBQ2xDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQ3RCLFNBQVMsQ0FBRSxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBRWxCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBRSwyQkFBMkI7WUFFL0QsSUFBRyxNQUFNLEtBQUssSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFHLHlDQUF5QztnQkFDM0UsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUM7Z0JBQzNCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLEtBQUssWUFBWSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztnQkFDckUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUMsU0FBUyxFQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7Z0JBRXBGLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFFLENBQUM7YUFDdEM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFlBQVksRUFBRSxXQUFXLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0lBRUQscUJBQXFCO0lBQ3JCLFVBQVUsQ0FBQyxHQUFVO1FBQ25CLElBQUksR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBQy9ELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLEdBQUMsRUFBRSxDQUFDO0lBQ2pDLENBQUM7O29IQWpEVSx1QkFBdUI7d0dBQXZCLHVCQUF1QixvUkNUcEMsNElBR0E7MkZETWEsdUJBQXVCO2tCQUxuQyxTQUFTOytCQUNFLGtCQUFrQjtnSUFNbkIsUUFBUTtzQkFBaEIsS0FBSztnQkFDRyxPQUFPO3NCQUFmLEtBQUs7Z0JBQ0csU0FBUztzQkFBakIsS0FBSztnQkFDRyxRQUFRO3NCQUFoQixLQUFLO2dCQUNHLFFBQVE7c0JBQWhCLEtBQUs7Z0JBRWUsTUFBTTtzQkFBMUIsU0FBUzt1QkFBQyxRQUFRIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2hhbmdlRGV0ZWN0b3JSZWYsIENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgUmVuZGVyZXIyLCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYsIE9uRGVzdHJveSwgQWZ0ZXJWaWV3SW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQW5hbG9nU2lnT2JzZXJ2ZXIgfSBmcm9tICdjcmVzLWNvbS13cmFwcGVyJztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdtMy1pbmRpY2F0b3ItYmFyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2luZGljYXRvci1iYXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9faW5kaWNhdG9yLWJhci5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIE0zSW5kaWNhdG9yQmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kge1xuXG4gIEBJbnB1dCgpIEZCU2lnbmFsOnN0cmluZyA9ICcnO1xuICBASW5wdXQoKSBGQkNsYXNzOnN0cmluZyA9ICdwcmltYXJ5JztcbiAgQElucHV0KCkgRGlyZWN0aW9uOiAnaG9yaXpvbnRhbCcgfCAndmVydGljYWwnID0gJ2hvcml6b250YWwnO1xuICBASW5wdXQoKSBNaW5WYWx1ZTogbnVtYmVyID0gMDtcbiAgQElucHV0KCkgTWF4VmFsdWU6IG51bWJlciA9IDY1NTM1O1xuXG4gIEBWaWV3Q2hpbGQoJ2Jhcm9iaicpIEJhck9iaiE6YW55OyBcblxuICBiYXJTdWJIYW5kZWw/OlN1YnNjcmlwdGlvbjtcbiAgbGFzdEJhclZhbHVlOm51bWJlciA9IC0xO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcmVuZGVyZXI6UmVuZGVyZXIyLCBwcml2YXRlIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0b3JSZWYpIHsgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuY2hhbmdlRGV0ZWN0aW9uLmRldGFjaCgpO1xuXG4gICAgXG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgdGhpcy5jaGFuZ2VEZXRlY3Rpb24uZGV0ZWN0Q2hhbmdlcygpO1xuXG4gICAgdGhpcy5iYXJTdWJIYW5kZWwgPSBBbmFsb2dTaWdPYnNlcnZlclxuICAgICAgLk9ic2VydmUodGhpcy5GQlNpZ25hbClcbiAgICAgIC5zdWJzY3JpYmUoICh2YWwpID0+IHtcblxuICAgICAgICBsZXQgbmV3VmFsID0gdGhpcy5zY2FsZVZhbHVlKHZhbCk7ICAvL2NvbnZlcnQgaXQgdG8gMC4wJS0xMDAuMCVcblxuICAgICAgICBpZihuZXdWYWwgIT09IHRoaXMubGFzdEJhclZhbHVlKSB7ICAvL29ubHkgcnVuIGNoYW5nZSBkZXRlY3Rpb24gb24gbmV3IHZhbHVlc1xuICAgICAgICAgIHRoaXMubGFzdEJhclZhbHVlID0gbmV3VmFsO1xuICAgICAgICAgIGxldCBkaXJlY3Rpb24gPSB0aGlzLkRpcmVjdGlvbiA9PT0gJ2hvcml6b250YWwnID8gJ3dpZHRoJyA6ICdoZWlnaHQnO1xuICAgICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5CYXJPYmoubmF0aXZlRWxlbWVudCxkaXJlY3Rpb24sYCR7dGhpcy5sYXN0QmFyVmFsdWV9JWApO1xuXG4gICAgICAgICAgdGhpcy5jaGFuZ2VEZXRlY3Rpb24uZGV0ZWN0Q2hhbmdlcygpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIFxuICB9XG4gIFxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICB0aGlzLmJhclN1YkhhbmRlbD8udW5zdWJzY3JpYmUoKTtcbiAgfVxuXG4gIC8vc2NhbGUgdG8gMC4wLTEwMC4wJVxuICBzY2FsZVZhbHVlKHZhbDpudW1iZXIpOm51bWJlciB7XG4gICAgbGV0IHBjdCA9ICh2YWwgLSB0aGlzLk1pblZhbHVlKS8odGhpcy5NYXhWYWx1ZSAtIHRoaXMuTWluVmFsdWUpXG4gICAgcmV0dXJuIE1hdGgucm91bmQocGN0KjEwMDApLzEwO1xuICB9XG59XG4iLCI8ZGl2IGNsYXNzPVwib2JqLWJhci1jb250YWluZXJcIiBbbmdDbGFzc109XCJEaXJlY3Rpb25cIj5cbiAgPGRpdiBjbGFzcz1cIm9iai1iYXItc3RhdGljc1wiICNiYXJvYmogW25nQ2xhc3NdPVwiRkJDbGFzc1wiPjwvZGl2PlxuPC9kaXY+XG4iXX0=