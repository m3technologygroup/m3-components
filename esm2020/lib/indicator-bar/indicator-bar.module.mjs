import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { M3IndicatorBarComponent } from "./indicator-bar.component";
import * as i0 from "@angular/core";
export class M3IndicatorBarModule {
}
M3IndicatorBarModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
M3IndicatorBarModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarModule, declarations: [M3IndicatorBarComponent], imports: [CommonModule], exports: [M3IndicatorBarComponent] });
M3IndicatorBarModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3IndicatorBarModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        M3IndicatorBarComponent
                    ],
                    imports: [
                        CommonModule
                    ],
                    exports: [
                        M3IndicatorBarComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kaWNhdG9yLWJhci5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9tMy1jb21wb25lbnRzL3NyYy9saWIvaW5kaWNhdG9yLWJhci9pbmRpY2F0b3ItYmFyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUcvQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQzs7QUFjcEUsTUFBTSxPQUFPLG9CQUFvQjs7aUhBQXBCLG9CQUFvQjtrSEFBcEIsb0JBQW9CLGlCQVQ3Qix1QkFBdUIsYUFHdkIsWUFBWSxhQUdaLHVCQUF1QjtrSEFHZCxvQkFBb0IsWUFQdEI7WUFDUCxZQUFZO1NBQ2I7MkZBS1Usb0JBQW9CO2tCQVhoQyxRQUFRO21CQUFDO29CQUNSLFlBQVksRUFBRTt3QkFDWix1QkFBdUI7cUJBQ3hCO29CQUNELE9BQU8sRUFBRTt3QkFDUCxZQUFZO3FCQUNiO29CQUNELE9BQU8sRUFBRTt3QkFDUCx1QkFBdUI7cUJBQ3hCO2lCQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcblxyXG5pbXBvcnQgeyBNM0luZGljYXRvckJhckNvbXBvbmVudCB9IGZyb20gXCIuL2luZGljYXRvci1iYXIuY29tcG9uZW50XCI7XHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIE0zSW5kaWNhdG9yQmFyQ29tcG9uZW50XHJcbiAgXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtcclxuICAgIE0zSW5kaWNhdG9yQmFyQ29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTTNJbmRpY2F0b3JCYXJNb2R1bGUgeyB9XHJcbiJdfQ==