import { Component, Input, ViewChild, HostListener } from '@angular/core';
import { DigitalSigObserver } from 'cres-com-wrapper';
import { trigger, style, animate, transition } from '@angular/animations';
import * as i0 from "@angular/core";
import * as i1 from "cres-com-wrapper";
import * as i2 from "@angular/common";
export class M3TrayButtonComponent {
    constructor() {
        this.Icon = "";
        this.Label = "";
        this.Signal = "";
        this.FBSignal = "";
        this.FbClass = 'primary';
        this.circleWidth = 0;
    }
    onResize(event) {
        this.calculateCircleWidth();
    }
    calculateCircleWidth() {
        this.circleWidth = this.circleContainer?.nativeElement.offsetHeight;
    }
    getCircleWidth() {
        return { "width": `${this.circleWidth}px` };
    }
    ngOnInit() {
        this.btnFb = DigitalSigObserver.Observe(this.FBSignal);
    }
    ngAfterViewInit() {
        this.calculateCircleWidth();
    }
    buildFbClass() {
        switch (this.FbClass) {
            case 'primary':
                return 'fb-primary';
            case 'accent':
                return 'fb-accent';
            case 'warn':
                return 'fb-warn';
            default:
                return this.FbClass;
        }
    }
}
M3TrayButtonComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
M3TrayButtonComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: M3TrayButtonComponent, selector: "m3-tray-button", inputs: { Icon: "Icon", Label: "Label", Signal: "Signal", FBSignal: "FBSignal", FbClass: "FbClass" }, host: { listeners: { "window:resize": "onResize($event)" } }, viewQueries: [{ propertyName: "circleContainer", first: true, predicate: ["circleContainer"], descendants: true }], ngImport: i0, template: "<button CresDigitalSend [Signal]=\"Signal\">\n  <div class=\"circle-container\" #circleContainer [ngStyle]=\"getCircleWidth()\">\n    <div class=\"circle\">\n      <svg viewBox=\"0 0 15 15\">\n        <text x=\"1\" y=\"14\">{{Icon}}</text>\n      </svg>\n    </div>\n    <div *ngIf=\"btnFb | async\" [@enterAnimation] [ngClass]=\"buildFbClass()\" class=\"feedback-circle\">\n\n    </div>\n  </div>\n  <h6>{{Label}}</h6>\n</button>", styles: ["button{height:100%;width:inherit;background-color:transparent;border:none;padding:5px;color:var(--m3-component-primary-contrast);display:flex;flex-direction:column;justify-content:space-between;align-items:center}button h6{margin-top:4px;height:25px}.circle{position:absolute;height:100%;width:100%;border-radius:100%;z-index:1;margin:auto;border:1px solid var(--m3-component-primary-contrast);display:flex;justify-content:center;align-items:center}.feedback-circle{position:absolute;height:100%;width:100%;border-radius:100%}.circle-container{height:calc(100% - 25px);position:relative}.fb-primary{background-color:var(--m3-component-primary)}.fb-accent{background-color:var(--m3-component-accent)}.fb-warn{background-color:var(--m3-component-warn)}svg{height:60%;fill:var(--m3-component-primary-contrast)}text{font-family:\"Material Icons\",sans-serif}h6{text-transform:uppercase}\n"], directives: [{ type: i1.CresDigitalSendDirective, selector: "[CresDigitalSend]", inputs: ["Signal", "PressHold"] }, { type: i2.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], pipes: { "async": i2.AsyncPipe }, animations: [
        trigger('enterAnimation', [
            transition(':enter', [
                style({ transform: 'translateY(10%) scale(.9,.9)', opacity: 0 }),
                animate('200ms ease-in-out', style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }))
            ]),
            transition(':leave', [
                style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }),
                animate('200ms ease-in-out', style({ transform: 'translateY(6%) scale(.9,.9)', opacity: 0 }))
            ])
        ])
    ] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonComponent, decorators: [{
            type: Component,
            args: [{ selector: 'm3-tray-button', animations: [
                        trigger('enterAnimation', [
                            transition(':enter', [
                                style({ transform: 'translateY(10%) scale(.9,.9)', opacity: 0 }),
                                animate('200ms ease-in-out', style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }))
                            ]),
                            transition(':leave', [
                                style({ transform: 'translateY(0) scale(1,1)', opacity: 1 }),
                                animate('200ms ease-in-out', style({ transform: 'translateY(6%) scale(.9,.9)', opacity: 0 }))
                            ])
                        ])
                    ], template: "<button CresDigitalSend [Signal]=\"Signal\">\n  <div class=\"circle-container\" #circleContainer [ngStyle]=\"getCircleWidth()\">\n    <div class=\"circle\">\n      <svg viewBox=\"0 0 15 15\">\n        <text x=\"1\" y=\"14\">{{Icon}}</text>\n      </svg>\n    </div>\n    <div *ngIf=\"btnFb | async\" [@enterAnimation] [ngClass]=\"buildFbClass()\" class=\"feedback-circle\">\n\n    </div>\n  </div>\n  <h6>{{Label}}</h6>\n</button>", styles: ["button{height:100%;width:inherit;background-color:transparent;border:none;padding:5px;color:var(--m3-component-primary-contrast);display:flex;flex-direction:column;justify-content:space-between;align-items:center}button h6{margin-top:4px;height:25px}.circle{position:absolute;height:100%;width:100%;border-radius:100%;z-index:1;margin:auto;border:1px solid var(--m3-component-primary-contrast);display:flex;justify-content:center;align-items:center}.feedback-circle{position:absolute;height:100%;width:100%;border-radius:100%}.circle-container{height:calc(100% - 25px);position:relative}.fb-primary{background-color:var(--m3-component-primary)}.fb-accent{background-color:var(--m3-component-accent)}.fb-warn{background-color:var(--m3-component-warn)}svg{height:60%;fill:var(--m3-component-primary-contrast)}text{font-family:\"Material Icons\",sans-serif}h6{text-transform:uppercase}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { circleContainer: [{
                type: ViewChild,
                args: ['circleContainer']
            }], Icon: [{
                type: Input
            }], Label: [{
                type: Input
            }], Signal: [{
                type: Input
            }], FBSignal: [{
                type: Input
            }], FbClass: [{
                type: Input
            }], onResize: [{
                type: HostListener,
                args: ['window:resize', ['$event']]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJheS1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbTMtY29tcG9uZW50cy9zcmMvbGliL3RyYXktYnV0dG9uL3RyYXktYnV0dG9uLmNvbXBvbmVudC50cyIsIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL20zLWNvbXBvbmVudHMvc3JjL2xpYi90cmF5LWJ1dHRvbi90cmF5LWJ1dHRvbi5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxTQUFTLEVBQWMsWUFBWSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUM3RyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUN0RCxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUF1QjFFLE1BQU0sT0FBTyxxQkFBcUI7SUFpQmhDO1FBYlMsU0FBSSxHQUFVLEVBQUUsQ0FBQztRQUNqQixVQUFLLEdBQVUsRUFBRSxDQUFBO1FBQ2pCLFdBQU0sR0FBVSxFQUFFLENBQUE7UUFDbEIsYUFBUSxHQUFVLEVBQUUsQ0FBQztRQUNyQixZQUFPLEdBQTJDLFNBQVMsQ0FBQztRQUVyRSxnQkFBVyxHQUFVLENBQUMsQ0FBQztJQU9QLENBQUM7SUFKakIsUUFBUSxDQUFDLEtBQVU7UUFDakIsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUlELG9CQUFvQjtRQUNsQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUUsYUFBYSxDQUFDLFlBQVksQ0FBQztJQUN0RSxDQUFDO0lBRUQsY0FBYztRQUNaLE9BQU8sRUFBQyxPQUFPLEVBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxJQUFJLEVBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxLQUFLLEdBQUcsa0JBQWtCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFFRCxZQUFZO1FBQ1YsUUFBUSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ3BCLEtBQUssU0FBUztnQkFDVixPQUFPLFlBQVksQ0FBQztZQUN4QixLQUFLLFFBQVE7Z0JBQ1QsT0FBTyxXQUFXLENBQUM7WUFDdkIsS0FBSyxNQUFNO2dCQUNMLE9BQU8sU0FBUyxDQUFDO1lBQ3ZCO2dCQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUN2QjtJQUNILENBQUM7O2tIQTlDVSxxQkFBcUI7c0dBQXJCLHFCQUFxQiw4VUN6QmxDLGdiQVlTLHF3Q0RKSztRQUNWLE9BQU8sQ0FDTCxnQkFBZ0IsRUFBRTtZQUNoQixVQUFVLENBQUMsUUFBUSxFQUFFO2dCQUNuQixLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsOEJBQThCLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDO2dCQUM5RCxPQUFPLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLEVBQUMsU0FBUyxFQUFFLDBCQUEwQixFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDO2FBQ3pGLENBQUM7WUFDRixVQUFVLENBQUMsUUFBUSxFQUFFO2dCQUNuQixLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsMEJBQTBCLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDO2dCQUMxRCxPQUFPLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLEVBQUMsU0FBUyxFQUFFLDZCQUE2QixFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDO2FBQzVGLENBQUM7U0FDSCxDQUNGO0tBQ0Y7MkZBSVUscUJBQXFCO2tCQW5CakMsU0FBUzsrQkFDRSxnQkFBZ0IsY0FDZDt3QkFDVixPQUFPLENBQ0wsZ0JBQWdCLEVBQUU7NEJBQ2hCLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0NBQ25CLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSw4QkFBOEIsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUM7Z0NBQzlELE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsMEJBQTBCLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7NkJBQ3pGLENBQUM7NEJBQ0YsVUFBVSxDQUFDLFFBQVEsRUFBRTtnQ0FDbkIsS0FBSyxDQUFDLEVBQUMsU0FBUyxFQUFFLDBCQUEwQixFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQztnQ0FDMUQsT0FBTyxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSw2QkFBNkIsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQzs2QkFDNUYsQ0FBQzt5QkFDSCxDQUNGO3FCQUNGOzBFQU02QixlQUFlO3NCQUE1QyxTQUFTO3VCQUFDLGlCQUFpQjtnQkFFbkIsSUFBSTtzQkFBWixLQUFLO2dCQUNHLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxNQUFNO3NCQUFkLEtBQUs7Z0JBQ0csUUFBUTtzQkFBaEIsS0FBSztnQkFDRyxPQUFPO3NCQUFmLEtBQUs7Z0JBS04sUUFBUTtzQkFEUCxZQUFZO3VCQUFDLGVBQWUsRUFBQyxDQUFDLFFBQVEsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmLCBIb3N0TGlzdGVuZXIsIEFmdGVyVmlld0luaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IERpZ2l0YWxTaWdPYnNlcnZlciB9IGZyb20gJ2NyZXMtY29tLXdyYXBwZXInO1xuaW1wb3J0IHsgdHJpZ2dlciwgc3R5bGUsIGFuaW1hdGUsIHRyYW5zaXRpb24gfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcblxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdtMy10cmF5LWJ1dHRvbicsXG4gIGFuaW1hdGlvbnM6IFtcbiAgICB0cmlnZ2VyKFxuICAgICAgJ2VudGVyQW5pbWF0aW9uJywgW1xuICAgICAgICB0cmFuc2l0aW9uKCc6ZW50ZXInLCBbXG4gICAgICAgICAgc3R5bGUoe3RyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMTAlKSBzY2FsZSguOSwuOSknLCBvcGFjaXR5OiAwfSksXG4gICAgICAgICAgYW5pbWF0ZSgnMjAwbXMgZWFzZS1pbi1vdXQnLCBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWSgwKSBzY2FsZSgxLDEpJywgb3BhY2l0eTogMX0pKVxuICAgICAgICBdKSxcbiAgICAgICAgdHJhbnNpdGlvbignOmxlYXZlJywgW1xuICAgICAgICAgIHN0eWxlKHt0cmFuc2Zvcm06ICd0cmFuc2xhdGVZKDApIHNjYWxlKDEsMSknLCBvcGFjaXR5OiAxfSksXG4gICAgICAgICAgYW5pbWF0ZSgnMjAwbXMgZWFzZS1pbi1vdXQnLCBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWSg2JSkgc2NhbGUoLjksLjkpJywgb3BhY2l0eTogMH0pKVxuICAgICAgICBdKVxuICAgICAgXVxuICAgIClcbiAgXSxcbiAgdGVtcGxhdGVVcmw6ICcuL3RyYXktYnV0dG9uLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vX3RyYXktYnV0dG9uLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTTNUcmF5QnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcblxuICBAVmlld0NoaWxkKCdjaXJjbGVDb250YWluZXInKSBjaXJjbGVDb250YWluZXI/OkVsZW1lbnRSZWY7XG5cbiAgQElucHV0KCkgSWNvbjpzdHJpbmcgPSBcIlwiO1xuICBASW5wdXQoKSBMYWJlbDpzdHJpbmcgPSBcIlwiXG4gIEBJbnB1dCgpIFNpZ25hbDpzdHJpbmcgPSBcIlwiXG4gIEBJbnB1dCgpIEZCU2lnbmFsOnN0cmluZyA9IFwiXCI7XG4gIEBJbnB1dCgpIEZiQ2xhc3M6ICdwcmltYXJ5JyB8ICdhY2NlbnQnIHwgJ3dhcm4nIHwgc3RyaW5nID0gJ3ByaW1hcnknO1xuICBidG5GYj86T2JzZXJ2YWJsZTxib29sZWFuPjtcbiAgY2lyY2xlV2lkdGg6bnVtYmVyID0gMDtcblxuICBASG9zdExpc3RlbmVyKCd3aW5kb3c6cmVzaXplJyxbJyRldmVudCddKVxuICBvblJlc2l6ZShldmVudD86YW55KSB7XG4gICAgdGhpcy5jYWxjdWxhdGVDaXJjbGVXaWR0aCgpO1xuICB9XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBjYWxjdWxhdGVDaXJjbGVXaWR0aCgpIHtcbiAgICB0aGlzLmNpcmNsZVdpZHRoID0gdGhpcy5jaXJjbGVDb250YWluZXI/Lm5hdGl2ZUVsZW1lbnQub2Zmc2V0SGVpZ2h0O1xuICB9XG5cbiAgZ2V0Q2lyY2xlV2lkdGgoKSB7XG4gICAgcmV0dXJuIHtcIndpZHRoXCI6YCR7dGhpcy5jaXJjbGVXaWR0aH1weGB9O1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5idG5GYiA9IERpZ2l0YWxTaWdPYnNlcnZlci5PYnNlcnZlKHRoaXMuRkJTaWduYWwpO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIHRoaXMuY2FsY3VsYXRlQ2lyY2xlV2lkdGgoKTtcbiAgfVxuXG4gIGJ1aWxkRmJDbGFzcygpIHtcbiAgICBzd2l0Y2ggKHRoaXMuRmJDbGFzcykge1xuICAgICAgY2FzZSAncHJpbWFyeSc6XG4gICAgICAgICAgcmV0dXJuICdmYi1wcmltYXJ5JztcbiAgICAgIGNhc2UgJ2FjY2VudCc6XG4gICAgICAgICAgcmV0dXJuICdmYi1hY2NlbnQnO1xuICAgICAgY2FzZSAnd2Fybic6XG4gICAgICAgICAgICByZXR1cm4gJ2ZiLXdhcm4nO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIHRoaXMuRmJDbGFzcztcbiAgICB9XG4gIH1cbn1cbiIsIjxidXR0b24gQ3Jlc0RpZ2l0YWxTZW5kIFtTaWduYWxdPVwiU2lnbmFsXCI+XG4gIDxkaXYgY2xhc3M9XCJjaXJjbGUtY29udGFpbmVyXCIgI2NpcmNsZUNvbnRhaW5lciBbbmdTdHlsZV09XCJnZXRDaXJjbGVXaWR0aCgpXCI+XG4gICAgPGRpdiBjbGFzcz1cImNpcmNsZVwiPlxuICAgICAgPHN2ZyB2aWV3Qm94PVwiMCAwIDE1IDE1XCI+XG4gICAgICAgIDx0ZXh0IHg9XCIxXCIgeT1cIjE0XCI+e3tJY29ufX08L3RleHQ+XG4gICAgICA8L3N2Zz5cbiAgICA8L2Rpdj5cbiAgICA8ZGl2ICpuZ0lmPVwiYnRuRmIgfCBhc3luY1wiIFtAZW50ZXJBbmltYXRpb25dIFtuZ0NsYXNzXT1cImJ1aWxkRmJDbGFzcygpXCIgY2xhc3M9XCJmZWVkYmFjay1jaXJjbGVcIj5cblxuICAgIDwvZGl2PlxuICA8L2Rpdj5cbiAgPGg2Pnt7TGFiZWx9fTwvaDY+XG48L2J1dHRvbj4iXX0=