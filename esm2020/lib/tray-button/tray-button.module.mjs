import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CresComWrapperModule } from "cres-com-wrapper";
import { M3TrayButtonComponent } from "./tray-button.component";
import * as i0 from "@angular/core";
export class M3TrayButtonModule {
}
M3TrayButtonModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
M3TrayButtonModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonModule, declarations: [M3TrayButtonComponent], imports: [CommonModule,
        CresComWrapperModule], exports: [M3TrayButtonComponent] });
M3TrayButtonModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonModule, imports: [[
            CommonModule,
            CresComWrapperModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: M3TrayButtonModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        M3TrayButtonComponent
                    ],
                    imports: [
                        CommonModule,
                        CresComWrapperModule
                    ],
                    exports: [
                        M3TrayButtonComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJheS1idXR0b24ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbTMtY29tcG9uZW50cy9zcmMvbGliL3RyYXktYnV0dG9uL3RyYXktYnV0dG9uLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUV4RCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQzs7QUFlaEUsTUFBTSxPQUFPLGtCQUFrQjs7K0dBQWxCLGtCQUFrQjtnSEFBbEIsa0JBQWtCLGlCQVYzQixxQkFBcUIsYUFHckIsWUFBWTtRQUNaLG9CQUFvQixhQUdwQixxQkFBcUI7Z0hBR1osa0JBQWtCLFlBUnBCO1lBQ1AsWUFBWTtZQUNaLG9CQUFvQjtTQUNyQjsyRkFLVSxrQkFBa0I7a0JBWjlCLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFO3dCQUNaLHFCQUFxQjtxQkFDdEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osb0JBQW9CO3FCQUNyQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AscUJBQXFCO3FCQUN0QjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IENyZXNDb21XcmFwcGVyTW9kdWxlIH0gZnJvbSBcImNyZXMtY29tLXdyYXBwZXJcIjtcclxuXHJcbmltcG9ydCB7IE0zVHJheUJ1dHRvbkNvbXBvbmVudCB9IGZyb20gXCIuL3RyYXktYnV0dG9uLmNvbXBvbmVudFwiO1xyXG5cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBNM1RyYXlCdXR0b25Db21wb25lbnRcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIENyZXNDb21XcmFwcGVyTW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBNM1RyYXlCdXR0b25Db21wb25lbnRcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNM1RyYXlCdXR0b25Nb2R1bGUgeyB9XHJcbiJdfQ==