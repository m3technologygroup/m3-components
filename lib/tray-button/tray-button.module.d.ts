import * as i0 from "@angular/core";
import * as i1 from "./tray-button.component";
import * as i2 from "@angular/common";
import * as i3 from "cres-com-wrapper";
export declare class M3TrayButtonModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<M3TrayButtonModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<M3TrayButtonModule, [typeof i1.M3TrayButtonComponent], [typeof i2.CommonModule, typeof i3.CresComWrapperModule], [typeof i1.M3TrayButtonComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<M3TrayButtonModule>;
}
