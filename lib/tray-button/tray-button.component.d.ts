import { OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export declare class M3TrayButtonComponent implements OnInit, AfterViewInit {
    circleContainer?: ElementRef;
    Icon: string;
    Label: string;
    Signal: string;
    FBSignal: string;
    FbClass: 'primary' | 'accent' | 'warn' | string;
    btnFb?: Observable<boolean>;
    circleWidth: number;
    onResize(event?: any): void;
    constructor();
    calculateCircleWidth(): void;
    getCircleWidth(): {
        width: string;
    };
    ngOnInit(): void;
    ngAfterViewInit(): void;
    buildFbClass(): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<M3TrayButtonComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<M3TrayButtonComponent, "m3-tray-button", never, { "Icon": "Icon"; "Label": "Label"; "Signal": "Signal"; "FBSignal": "FBSignal"; "FbClass": "FbClass"; }, {}, never, never>;
}
