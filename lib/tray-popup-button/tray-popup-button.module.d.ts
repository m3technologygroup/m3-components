import * as i0 from "@angular/core";
import * as i1 from "./tray-popup-button.component";
import * as i2 from "@angular/common";
import * as i3 from "cres-com-wrapper";
export declare class M3TrayPopupButtonModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<M3TrayPopupButtonModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<M3TrayPopupButtonModule, [typeof i1.M3TrayPopupButtonComponent], [typeof i2.CommonModule, typeof i3.CresComWrapperModule], [typeof i1.M3TrayPopupButtonComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<M3TrayPopupButtonModule>;
}
