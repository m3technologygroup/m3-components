import * as i0 from "@angular/core";
import * as i1 from "./indicator-bar.component";
import * as i2 from "@angular/common";
export declare class M3IndicatorBarModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<M3IndicatorBarModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<M3IndicatorBarModule, [typeof i1.M3IndicatorBarComponent], [typeof i2.CommonModule], [typeof i1.M3IndicatorBarComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<M3IndicatorBarModule>;
}
