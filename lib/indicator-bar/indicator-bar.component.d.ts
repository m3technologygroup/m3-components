import { ChangeDetectorRef, OnInit, Renderer2, OnDestroy, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import * as i0 from "@angular/core";
export declare class M3IndicatorBarComponent implements OnInit, AfterViewInit, OnDestroy {
    private renderer;
    private changeDetection;
    FBSignal: string;
    FBClass: string;
    Direction: 'horizontal' | 'vertical';
    MinValue: number;
    MaxValue: number;
    BarObj: any;
    barSubHandel?: Subscription;
    lastBarValue: number;
    constructor(renderer: Renderer2, changeDetection: ChangeDetectorRef);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    scaleValue(val: number): number;
    static ɵfac: i0.ɵɵFactoryDeclaration<M3IndicatorBarComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<M3IndicatorBarComponent, "m3-indicator-bar", never, { "FBSignal": "FBSignal"; "FBClass": "FBClass"; "Direction": "Direction"; "MinValue": "MinValue"; "MaxValue": "MaxValue"; }, {}, never, never>;
}
